﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class GeneratePlayerGrowthRatesAndCaps
{
    /// <summary>
    /// Used to determine how asset and flaw affect growth rates of custom character
    /// </summary>
    /// <returns></returns>
    private static Dictionary<string, (Dictionary<string, float>, Dictionary<string, float>)> customPlayerGrowthModifications = new Dictionary<string, (Dictionary<string, float>, Dictionary<string, float>)>
    {
        { "HP", (
            new Dictionary<string, float>
            {
                { "HP", .3f },
                { "Def", .05f },
                { "Res", .05f },
            },
            new Dictionary<string, float>
            {
                {"HP", -.2f},
                {"Def", -.05f},
                {"Res", -.05f},
            })
        },
        { "Str", (
            new Dictionary<string, float>
            {
                { "HP", .1f },
                { "Str", .15f },
                { "Skl", .05f },
            },
            new Dictionary<string, float>
            {
                {"HP", -.1f},
                {"Str", -.1f},
                {"Skl", -.05f},
            })
        },
        { "Mag", (
            new Dictionary<string, float>
            {
                { "Mag", .15f },
                { "Skl", .05f },
                { "Res", .05f },
            },
            new Dictionary<string, float>
            {
                {"Mag", -.1f},
                {"Skl", -.05f},
                {"Res", -.05f},
            })
        },
        { "Def", (
            new Dictionary<string, float>
            {
                { "Def", .15f },
                { "HP", .1f },
                { "Str", .05f },
            },
            new Dictionary<string, float>
            {
                {"Def", -.1f},
                {"HP", -.1f},
                {"Str", -.05f},
            })
        },
        { "Res", (
            new Dictionary<string, float>
            {
                { "Mag", .05f },
                { "Spd", .05f },
                { "Res", .15f },
            },
            new Dictionary<string, float>
            {
                {"Mag", -.05f},
                {"Spd", -.05f},
                {"Res", -.1f},
            })
        },
        { "Skl", (
            new Dictionary<string, float>
            {
                { "Skl", .15f },
                { "Str", .05f },
                { "Spd", .05f },
            },
            new Dictionary<string, float>
            {
                {"Skl", -.1f},
                {"Str", -.05f},
                {"Spd", -.05f},
            })
        },
        { "Spd", (
            new Dictionary<string, float>
            {
                { "Spd", .15f },
                { "Skl", .05f },
                { "Res", .05f },
            },
            new Dictionary<string, float>
            {
                {"Spd", -.1f},
                {"Skl", -.05f},
                {"Res", -.05f},
            })
        },
    };

    private static Dictionary<string, (int, int)> customPlayerStatBaseModifications = new Dictionary<string, (int, int)>
    {
        { "HP", (5, -3) },
        { "Str", (2, -1) },
        { "Mag", (2, -1) },
        { "Def", (2, -1) },
        { "Res", (2, -1) },
        { "Skl", (2, -1) },
        { "Spd", (2, -1) },
    };

    private static Dictionary<string, (Dictionary<string, int>, Dictionary<string, int>)> customPlayerStatCapModifications = new Dictionary<string, (Dictionary<string, int>, Dictionary<string, int>)>
    {
        { "HP", (
            new Dictionary<string, int>
            {
                { "HP", 8 },
                { "Res", 2 },
                { "Def", 2 },
            },
            new Dictionary<string, int>
            {
                {"HP", -6},
                {"Res", -1},
                {"Def", -1},
            })
        },
        { "Str", (
            new Dictionary<string, int>
            {
                { "Str", 4 },
                { "Skl", 2 },
                { "HP", 4 },
            },
            new Dictionary<string, int>
            {
                {"Str", -3},
                {"Skl", -1},
                {"HP", -3},
            })
        },
        { "Mag", (
            new Dictionary<string, int>
            {
                { "Mag", 4 },
                { "Skl", 2 },
                { "Res", 2 },
            },
            new Dictionary<string, int>
            {
                {"Mag", -3},
                {"Skl", -1},
                {"Res", -1},
            })
        },
        { "Def", (
            new Dictionary<string, int>
            {
                { "Def", 4 },
                { "Str", 2 },
                { "HP", 4 },
            },
            new Dictionary<string, int>
            {
                {"Def", -3},
                {"Str", -1},
                {"HP", -3},
            })
        },
        { "Res", (
            new Dictionary<string, int>
            {
                { "Res", 4 },
                { "Mag", 2 },
                { "Spd", 2 },
            },
            new Dictionary<string, int>
            {
                {"Res", -3},
                {"Mag", -1},
                {"Spd", -1},
            })
        },
        { "Skl", (
            new Dictionary<string, int>
            {
                { "Skl", 4 },
                { "Str", 2 },
                { "Spd", 2 },
            },
            new Dictionary<string, int>
            {
                {"Skl", -3},
                {"Str", -1},
                {"Spd", -1},
            })
        },
        { "Spd", (
            new Dictionary<string, int>
            {
                { "Spd", 4 },
                { "Skl", 2 },
                { "Res", 2 },
            },
            new Dictionary<string, int>
            {
                {"Spd", -3},
                {"Skl", -1},
                {"Res", -1},
            })
        },
    };

    private static GrowthRates GenerateGrowthRates(string asset, string flaw)
    {
        var assetRates = customPlayerGrowthModifications[asset].Item1;
        var flawRates = customPlayerGrowthModifications[flaw].Item2;

        assetRates.TryGetValue("HP", out var assetHP);
        assetRates.TryGetValue("Str", out var assetStr);
        assetRates.TryGetValue("Mag", out var assetMag);
        assetRates.TryGetValue("Def", out var assetDef);
        assetRates.TryGetValue("Res", out var assetRes);
        assetRates.TryGetValue("Skl", out var assetSkl);
        assetRates.TryGetValue("Spd", out var assetSpd);

        flawRates.TryGetValue("HP", out var flawHP);
        flawRates.TryGetValue("Str", out var flawStr);
        flawRates.TryGetValue("Mag", out var flawMag);
        flawRates.TryGetValue("Def", out var flawDef);
        flawRates.TryGetValue("Res", out var flawRes);
        flawRates.TryGetValue("Skl", out var flawSkl);
        flawRates.TryGetValue("Spd", out var flawSpd);

        // Ideally, all growth rates (including class) will total around 350% (evenly spread, each stat would be ~58%)
        return GrowthRates.CreateInstance(
            .6f + assetHP + flawHP,
            .4f + assetStr + flawStr,
            .4f + assetMag + flawMag,
            .4f + assetDef + flawDef,
            .4f + assetRes + flawRes,
            .4f + assetSkl + flawSkl,
            .4f + assetSpd + flawSpd
        );
    }

    private static StatSpread GenerateStatSpreads(string asset, string flaw)
    {
        var assetCaps = customPlayerStatCapModifications[asset].Item1;
        var flawCaps = customPlayerStatCapModifications[flaw].Item2;

        assetCaps.TryGetValue("HP", out var assetHP);
        assetCaps.TryGetValue("Str", out var assetStr);
        assetCaps.TryGetValue("Mag", out var assetMag);
        assetCaps.TryGetValue("Def", out var assetDef);
        assetCaps.TryGetValue("Res", out var assetRes);
        assetCaps.TryGetValue("Skl", out var assetSkl);
        assetCaps.TryGetValue("Spd", out var assetSpd);

        flawCaps.TryGetValue("HP", out var flawHP);
        flawCaps.TryGetValue("Str", out var flawStr);
        flawCaps.TryGetValue("Mag", out var flawMag);
        flawCaps.TryGetValue("Def", out var flawDef);
        flawCaps.TryGetValue("Res", out var flawRes);
        flawCaps.TryGetValue("Skl", out var flawSkl);
        flawCaps.TryGetValue("Spd", out var flawSpd);

        return StatSpread.CreateInstance(
            assetHP + flawHP,
            assetStr + flawStr,
            assetMag + flawMag,
            assetDef + flawDef,
            assetRes + flawRes,
            assetSkl + flawSkl,
            assetSpd + flawSpd,
            0
        );
    }

    [MenuItem("Assets/Generate Custom Character GrowthRates")]
    static void CreateAssets()
    {
        foreach (var asset in customPlayerGrowthModifications.Keys)
        {
            foreach (var flaw in customPlayerGrowthModifications.Keys)
            {
                if (asset != flaw)
                {
                    var rates = GenerateGrowthRates(asset, flaw);
                    string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath($"Assets/_RefData/GrowthRates/characterRates_Asset_{asset}_Flaw_{flaw}.asset");
                    AssetDatabase.CreateAsset(rates, assetPathAndName);

                    var caps = GenerateStatSpreads(asset, flaw);
                    assetPathAndName = AssetDatabase.GenerateUniqueAssetPath($"Assets/_RefData/StatSpread/characterCaps_Asset_{asset}_Flaw_{flaw}.asset");
                    AssetDatabase.CreateAsset(caps, assetPathAndName);

                    AssetDatabase.SaveAssets();
                    AssetDatabase.Refresh();
                    EditorUtility.FocusProjectWindow();
                }
            }
        }
    }
}
