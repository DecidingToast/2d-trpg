﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

[Adapter(typeof(string), typeof(string))]
public class StringHashcodeAdapter : IAdapter
{
    public object Convert(object valueIn, AdapterOptions options) => Math.Abs(valueIn.GetHashCode()).ToString("D8");
}
