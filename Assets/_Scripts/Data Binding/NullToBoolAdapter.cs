﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

[Adapter(typeof(object), typeof(bool))]
internal class NullToBoolAdapter : IAdapter
{
    public object Convert(object valueIn, AdapterOptions options)
    {
        return valueIn != null;
    }
}
