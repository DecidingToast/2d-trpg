﻿using Pathfinding;
using System;
using System.Threading.Tasks;
using UnityEngine;

internal class PortalInteractable : Interactable
{
    [SerializeField]
    private string _name = "Enter portal";
    public override string Name
    {
        get
        {
            return _name;
        }
    }
    [SerializeField]
    private PortalInteractable otherSide;
    private Transform activationSpace;

    void Start()
    {
        activationSpace = transform.GetChild(0);
    }

    public override bool CharacterCanInteract(ACharacter c)
    {
        return !_boardManager.OccupiedNodes.Contains(AstarData.active.data.gridGraph.GetNearest(otherSide.activationSpace.position).node) &&
            base.CharacterCanInteract(c);
    }

    public override Task InteractAsync(ACharacter c)
    {
        c.TeleportTo(AstarData.active.data.gridGraph.GetNearest(otherSide.activationSpace.position).node);
        return Task.FromResult(0);
    }
}
