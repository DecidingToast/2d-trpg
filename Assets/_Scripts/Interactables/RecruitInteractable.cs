﻿using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

internal class RecruitInteractable : Interactable
{

    public override string Name
    {
        get
        {
            return "Talk to " + GetComponent<Character>().Name;
        }
    }

    [SerializeField]
    private TextAsset script;

    // Array of characters, listed by name, who can speak with this interactable.
    [SerializeField]
    private string[] characterNames;

    [Inject]
    private AControlStack panelStack;

    public override bool CharacterCanInteract(ACharacter c)
    {
        var characterListed = false;
        foreach (var name in characterNames)
        {
            if (name == c.Name) characterListed = true;
        }
        return characterListed && base.CharacterCanInteract(c);
    }

    public override Task InteractAsync(ACharacter c)
    {
        return panelStack.PushContext<Dialogue>(new object[] { script, c, GetComponent<Character>() })
        .ContinueWith(antecedent =>
            {
                if (GetComponent<Character>())
                    _boardManager.SwitchTeams(GetComponent<Character>());
                c.HasMoved = true;
                Destroy(this);
            }, TaskContinuationOptions.ExecuteSynchronously);
    }
}
