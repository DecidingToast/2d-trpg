﻿using System.Threading.Tasks;
using Pathfinding;
using UnityEngine;
using Zenject;

public abstract class Interactable : DataBindMonobehaviour
{
    public abstract string Name { get; }

    [Inject]
    protected ABoardManager _boardManager;

    public virtual bool CharacterCanInteract(ACharacter c)
    {
        return AstarData.active.data.gridGraph.Distance(_boardManager.GetNodeFromInteractable(this), _boardManager.GetNodeFromCharacter(c)) == 1;
    }

    /// <summary>
    /// Activates this interactable, using the character interacting with it as an argument.
    /// Responsible for saying the character has already moved if this should end a character's turn.
    /// </summary>
    public abstract Task InteractAsync(ACharacter c);
}