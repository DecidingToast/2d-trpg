

using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

internal class EnterVehicle : Interactable
{
    private Vehicle vehicle;

    public override string Name
    {
        get
        {
            return "Enter Vehicle";
        }
    }

    public override Task InteractAsync(ACharacter c)
    {
        if (!vehicle)
        {
            throw new Exception("EnterVehicle should not be attached to a non-vehicle gameobject");
        }
        c.HasMoved = true;
        // Should vehicles be usable the turn you enter them?
        vehicle.HasMoved = false;
        _boardManager.RemoveCharacter(c);
        vehicle.Pilot = c;
        c.gameObject.SetActive(false);
        return Task.FromResult(0);
    }

    private void Start()
    {
        vehicle = GetComponent<Vehicle>();
    }
}