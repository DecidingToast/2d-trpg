﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using ProtoBuf;

internal class ProtobufSerializationTest : MonoBehaviour
{
    [Inject]
    private ICharacterModel characterModel;

    [Inject]
    private ISaveLoadCoordinator saveLoadCoordinator;

    [SerializeField]
    private CharacterStats stats;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            characterModel.PlayerCharacters.Add(stats);
            saveLoadCoordinator.Save();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            characterModel.PlayerCharacters.Clear();
            stats = new CharacterStats();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            saveLoadCoordinator.Load();
            stats = characterModel.PlayerCharacters[0];
        }
    }
}
