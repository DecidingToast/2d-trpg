﻿using UnityEngine;

internal class SortingOrder : MonoBehaviour
{
    public int offset = 0;

    int[] initialOrders;
    Renderer[] rend;

    void Start()
    {
        rend = GetComponentsInChildren<Renderer>();
        initialOrders = new int[rend.Length];
        for (int i = 0; i < rend.Length; i++)
        {
            initialOrders[i] = rend[i].sortingOrder;
        }
    }

    void FixedUpdate()
    {
        for (int i = 0; i < rend.Length; i++)
        {
            rend[i].sortingOrder = (initialOrders[i] + (int)(-transform.position.z * 10)) + offset;
        }
    }
}
