﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class SetAsMainCamera : MonoBehaviour
{
    [SerializeField]
    private bool dontDestroyOnLoad;

    // Start is called before the first frame update
    void Start()
    {
        var oldMain = Camera.main;
        var thisCamera = GetComponent<Camera>();
        var mainTag = oldMain.tag;
        oldMain.tag = thisCamera.tag;
        thisCamera.tag = mainTag;
    }
}
