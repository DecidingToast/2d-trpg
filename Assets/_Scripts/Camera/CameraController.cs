﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;
using BansheeGz.BGSpline.Curve;
using BansheeGz.BGSpline.Components;

internal class CameraController : MonoBehaviour
{
    [SerializeField]
    private Transform lookAt;

    [SerializeField]
    private float[] distances;
    private int currDistance = 0;
    private bool inProgress = false;

    [SerializeField]
    private float zoomTime = .25f;

    [SerializeField]
    private BGCcCursor cursor;

    private Camera cam;

    void Start()
    {
        cursor.DistanceRatio = 0;
        cam = Camera.main;
        cam.transform.position = cursor.CalculatePosition();
    }

    void Update()
    {
        if (!inProgress && Input.GetButtonDown("Back"))
        {
            StartCoroutine(Zoom((currDistance + 1) % distances.Length));
        }

        cam.transform.position = cursor.CalculatePosition();
        cam.transform.LookAt(lookAt);
    }

    private IEnumerator Zoom(int distance)
    {
        inProgress = true;
        float time = 0f;
        float current = distances[currDistance];
        float target = distances[distance];

        while (time < zoomTime)
        {
            time += Time.deltaTime;
            cursor.DistanceRatio = Mathf.Lerp(current, target, time / zoomTime);
            // cam.transform.eulerAngles = cursor.CalculateTangent();
            yield return null;
        }
        currDistance = distance;
        inProgress = false;
    }
}
