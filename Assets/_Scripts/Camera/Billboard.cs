﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class Billboard : MonoBehaviour {

    private Camera _cam;
    // Use this for initialization
    void Start()
    {
        _cam = Camera.main;
    }
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(transform.position + _cam.transform.rotation * Vector3.forward,
                         _cam.transform.rotation * Vector3.up);
	}
}
