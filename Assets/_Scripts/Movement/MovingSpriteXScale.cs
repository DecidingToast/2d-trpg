﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class MovingSpriteXScale : MonoBehaviour
{
    // Update is called once per frame
    private float lastX;

    void Start()
    {
        lastX = transform.position.x;
    }

    void Update()
    {
        var velocity = (transform.position.x - lastX) / Time.deltaTime;
        if (velocity < 0.1f)
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        else if (velocity > 0.1f)
            transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        lastX = transform.position.x;
    }
}
