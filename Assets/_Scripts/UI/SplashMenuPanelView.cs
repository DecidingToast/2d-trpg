﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityWeld.Binding;
using Zenject;

[Binding]
internal class SplashMenuPanelView : NavigationControlContext
{
    [Binding]
    public void GoToMainMenu()
    {
        contextStack.PushContext<MainMenuPanelView>();
    }
}
