using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

/// <summary>
/// Button to be used inside panelviews that have buttons, sends cancel event up to panel.
/// </summary>
internal class SelectableButton : Button, ICancelHandler
{
    [SerializeField]
    public ControlContext panelView;

    public bool Selected { get; set; }

    public void OnCancel(BaseEventData eventData)
    {
        if (panelView != null)
        {
            panelView.OnCancel(eventData);
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (Selected)
        {
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(gameObject);
            StartCoroutine(WaitOneFrameThenSelect());
        }
    }

    private IEnumerator WaitOneFrameThenSelect()
    {
        yield return null;
        Select();
    }
}