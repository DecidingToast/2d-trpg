﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Wraps a unity inputfield to better handle gamepad input
/// </summary>
public class InputFieldWrapper : Selectable, ISubmitHandler
{
    [SerializeField]
    private bool selectOnEnable = false;
    [SerializeField]
    private TMP_InputField inputField;

    protected override void Start()
    {
        base.Start();
        inputField.navigation = new Navigation { mode = Navigation.Mode.None };
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (selectOnEnable)
        {
            Select();
        }
    }

    public void OnSubmit(BaseEventData eventData)
    {
        inputField.Select();
    }
}
