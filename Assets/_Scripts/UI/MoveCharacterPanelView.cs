﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pathfinding;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

/// <summary>
/// This is the default state for the player's turn
/// </summary>
internal class MoveCharacterPanelView : CursorControllerPanelView
{
    private ACharacter character;
    public ACharacter Character
    {
        get { return character; }
        private set
        {
            SetProperty(ref character, value, "Character");
            oldPosition = null;
        }
    }

    [Inject]
    private ATurnState turnManager;

    [Inject]
    private AObjectPool<GameObject> prefabPool;

    protected GameObject[] nodeDisplay;

    private GraphNode oldPosition;


    public override async Task<ControlContext> Init(object initArgs)
    {
        Character = null;
        return await base.Init(initArgs);
    }

    public override void OnSubmit(BaseEventData eventData)
    {
        if (Character == null)
        {
            var c = boardManager.GetCharacterFromNode(Node);
            if (c != null && c.Team == Team.PLAYER && !c.HasMoved)
            {
                Character = c;
            }
            if (c != null && !c.HasMoved)
            {
                ShowRange();
            }
            Debug.Log("Character selected");
        }
        else
        {
            var walkable = cursor.Node.Walkable;
            var unoccupied = !boardManager.OccupiedNodes.Contains(cursor.Node);
            var reachable = character.GetReachableTiles().Contains(cursor.Node);
            if (cursor.Node == boardManager.GetNodeFromCharacter(Character) ||
                walkable && unoccupied && reachable)
            {
                enabled = false;
                HideRange();
                oldPosition = boardManager.GetNodeFromCharacter(Character);
                Character.MoveTo(Node)
                .ContinueWith(antecedent =>
                {
                    enabled = true;
                    contextStack.PushContext<CharacterActionsPanelView>(Character);
                }, TaskContinuationOptions.ExecuteSynchronously);
                Debug.Log("Character moved");
            }
        }
    }

    public override void OnCancel(BaseEventData eventData)
    {
        if (Character)
        {
            CancelMoveCursor();
            Node = boardManager.GetNodeFromCharacter(Character) as GridNodeBase;
            cursor.transform.position = (Vector3)Node.position;
            cursor.Node = Node;
            HideRange();
            Character = null;
        }
    }

    // TODO: Check if all characters have moved, end turn if so.
    protected override void OnEnable()
    {
        // This context is the "neutral" state of the player's turn. If we get here and the turn is over, we need to start the enemy turn.
        // This context will stay on the stack, so when the enemy turn pops itself off the stack, we should be back on the player's turn.
        if (boardManager.GetAllCharacters(Team.PLAYER).All(c => c.HasMoved))
        {
            // Set all player characters' hasMoved to false so they can move on the next turn.
            boardManager.GetAllCharacters(Team.PLAYER).ForEach(c =>
            {
                c.HasMoved = false;
            });
            contextStack.PushContext<EnemyTurn>();
            return;
        }

        base.OnEnable();
        turnManager.Team = Team.PLAYER;
        if (Character)
        {
            cursor.Node = boardManager.GetNodeFromCharacter(Character);
            ShowRange();
        }
        Node = cursor.Node as GridNodeBase;
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        HideRange();
    }

    public override void PopTo()
    {
        if (oldPosition != null && Character != null)
        {
            Character.TeleportTo(oldPosition);
            cursor.transform.position = Character.transform.position;
            Node = oldPosition as GridNodeBase;
        }
    }

    protected void ShowRange()
    {
        var nodes = Character.GetReachableTiles();
        int i = 0;
        nodeDisplay = prefabPool.Retrieve(nodes.Count());
        foreach (var node in nodes)
        {
            nodeDisplay[i].transform.position = (Vector3)node.position;
            nodeDisplay[i].SetActive(true);
            ++i;
        }
    }

    private void HideRange()
    {
        if (nodeDisplay != null)
        {
            prefabPool.Release(nodeDisplay);
            nodeDisplay = null;
        }
    }

    protected override bool ShowNodePredicate(GridNodeBase node)
    {
        return node.Walkable && !boardManager.OccupiedNodes.Contains(node);
    }
}
