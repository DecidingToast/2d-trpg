﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityWeld.Binding;

// TODO: Make this a panelview, make the LoadScript a promise.
[RequireComponent(typeof(AudioSource))]
[Binding]
internal class Dialogue : ControlContext
{

    [SerializeField]
    AudioClip sound;

    [SerializeField]
    private int framesBetweenLetters = 5;

    private AudioSource source;

    string[] currLine;

    private bool showDialog;
    [Binding]
    public bool ShowDialog
    {
        get { return showDialog; }
        private set { SetProperty(ref showDialog, value, "ShowDialog"); }
    }

    private string dialog;
    [Binding]
    public string Dialog
    {
        get { return dialog; }
        private set { SetProperty(ref dialog, value, "Dialog"); }
    }

    private ACharacter char1, char2;
    [Binding]
    public ACharacter Character1
    {
        get { return char1; }
        private set { SetProperty(ref char1, value, "Character1"); }
    }

    [Binding]
    public ACharacter Character2
    {
        get { return char2; }
        private set { SetProperty(ref char2, value, "Character2"); }
    }

    private Queue<string> _script;

    protected void Start()
    {
        source = GetComponent<AudioSource>();
    }

    public override async Task<ControlContext> Init(object initArgs)
    {
        var input = initArgs as object[];

        var script = input[0] as TextAsset;
        Character1 = input[1] as ACharacter;
        Character2 = input[2] as ACharacter;

        _script = new Queue<string>(script.text.Split(new string[] { "\n", "\r", "\r\n" }, StringSplitOptions.RemoveEmptyEntries));
        return await base.Init(initArgs);
    }

    /// <summary>
    /// Displays the next line, or closes the dialogue if there isn't one.
    /// Lots of yield return nulls due to waiting for input in specific frames.
    /// </summary>
    private IEnumerator ReadScript(Action callback)
    {
        ShowDialog = true;
        yield return new WaitForSeconds(1f);
        while (_script.Count > 0)
        {
            yield return null;
            currLine = _script.Dequeue().Split(':');
            var name = currLine[0];
            var line = currLine[1];

            Dialog = " " + name + ":\n";
            foreach (char c in line)
            {
                // Display letter-by-letter
                Dialog += c;
                if (sound) source.PlayOneShot(sound);
                // Wait x frames to display next letter
                for (int i = 0; i < framesBetweenLetters; i++)
                {
                    if (Input.GetButtonDown("A"))
                    {
                        // Autocomplete line if user chooses to.
                        Dialog = " " + name + ":\n" + line;
                        goto ENDLINE;
                    }
                    yield return null;
                }
            }
        ENDLINE:
            yield return null;
            // Wait for user to start next line.
            yield return new WaitUntil(() => Input.GetButtonDown("A"));
        }
        if (callback != null)
            callback();
        ShowDialog = false;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        StartCoroutine(ReadScript(() =>
        {
            contextStack.ClearContexts();
            contextStack.PushContext<MoveCharacterPanelView>();
        }));
    }
}
