﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityWeld.Binding;
using Zenject;

[Binding]
internal class MainMenuPanelView : NavigationControlContext
{
    [Inject]
    private ISaveLoadCoordinator saveLoadCoordinator;
    [Inject]
    private LoadLevelScreenBlocker loadLevelScreenBlocker;

    private bool showLoadSceneCurtain;
    [Binding]
    public bool ShowLoadSceneCurtain
    {
        get { return showLoadSceneCurtain; }
        private set { SetProperty(ref showLoadSceneCurtain, value, nameof(ShowLoadSceneCurtain)); }
    }

    [Binding]
    public void LoadGame()
    {
        saveLoadCoordinator.Load();
        // TODO: change to open a listview of available save files?
    }

    [Binding]
    public void SaveGame()
    {
        saveLoadCoordinator.Save();
    }

    [Binding]
    public void LoadLevel(string sceneName)
    {
        try
        {
            loadLevelScreenBlocker.LoadScene(sceneName);
        }
        catch (Exception e)
        {
            Debug.LogError($"Error loading scene {sceneName}: {e}");
        }
    }

    public override void OnCancel(BaseEventData eventData)
    {
        contextStack.PopContext();
    }
}

