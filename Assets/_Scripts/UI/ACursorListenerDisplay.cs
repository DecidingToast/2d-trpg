﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;
using Zenject;

[Binding]
internal abstract class ACursorListenerDisplay : DataBindMonobehaviour
{
    [SerializeField]
    private List<CursorControllerPanelView> cursors;
    public List<CursorControllerPanelView> Cursors
    {
        get { return cursors; }
        set { cursors = value; }
    }

    [Inject]
    AControlStack controlStack;

    void Start()
    {
        controlStack.ContextChanged += AControlStack_ContextChanged;
        var currentCursor = controlStack.CurrentContext as CursorControllerPanelView;
        if (currentCursor != null && Cursors.Contains(currentCursor))
        {
            currentCursor.OnHoverCharacter += OnHoverCharacter;
        }
    }

    void OnDestroy()
    {
        controlStack.ContextChanged -= AControlStack_ContextChanged;
    }

    private void AControlStack_ContextChanged(object sender, AControlStack.ContextChangeEventArgs args)
    {
        var arg1AsCursor = args?.OldContext as CursorControllerPanelView;
        var arg2AsCursor = args?.NewContext as CursorControllerPanelView;
        if (arg1AsCursor != null && Cursors.Contains(arg1AsCursor))
        {
            arg1AsCursor.OnHoverCharacter -= OnHoverCharacter;
        }
        if (arg2AsCursor != null && Cursors.Contains(arg2AsCursor))
        {
            arg2AsCursor.OnHoverCharacter += OnHoverCharacter;
        }
        CursorChanged(arg2AsCursor);
    }

    protected virtual void CursorChanged(CursorControllerPanelView cursor) { }

    protected abstract void OnHoverCharacter(ACharacter c);
}
