﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
internal class StatDeltaString : MonoBehaviour
{
    [SerializeField]
    private string format;

    private Text textComponent;
    private Text TextComponent
    {
        get
        {
            if (!textComponent)
            {
                textComponent = GetComponent<Text>();
            }
            return textComponent;
        }
    }

    private int statBase;
    public int StatBase
    {
        get { return statBase; }
        set
        {
            statBase = value;
            UpdateText();
        }
    }

    private int statCurrent;
    public int StatCurrent
    {
        get { return statCurrent; }
        set
        {
            statCurrent = value;
            UpdateText();
        }
    }

    private void UpdateText()
    {
        if (StatBase == StatCurrent)
        {
            TextComponent.text = StatBase.ToString();
        }
        else
        {
            TextComponent.text = string.Format(format, StatBase, StatCurrent);
        }
    }

}
