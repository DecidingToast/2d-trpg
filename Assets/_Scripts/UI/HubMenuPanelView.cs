﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;
using Zenject;

/// <summary>
/// Menu shown once a player has loaded a savegame. Contains options to select a level and manage units/inventory
/// </summary>
[Binding]
internal class HubMenuPanelView : NavigationControlContext
{

}
