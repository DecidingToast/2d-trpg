﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

internal class CombatText : MonoBehaviour
{

    private ACharacter character;

    // [Inject]
    // private CharacterDamageSignal _damageSignal;

    void Start()
    {
        character = GetComponentInParent<ACharacter>();
    }

    // void OnEnable()
    // {
    //     _damageSignal += reactToDamage;
    // }

    // void OnDisable()
    // {
    //     _damageSignal -= reactToDamage;
    // }

    private void reactToDamage(ACharacter c, int damage)
    {
        if (c == character)
        {
            PlayAnim(damage.ToString());
        }
    }

    public void PlayAnim(string text)
    {
        GetComponent<Text>().text = text;
        GetComponent<Animator>().SetTrigger("Hit");
    }
}
