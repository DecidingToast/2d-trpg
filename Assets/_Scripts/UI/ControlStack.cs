﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityWeld.Binding;
using System.Threading.Tasks;

[Binding]
internal class ControlStack : AControlStack
{
    [SerializeField]
    private ControlContext startingView;

    private ObservableList<ControlContext> contexts = new ObservableList<ControlContext>();
    public override ObservableList<ControlContext> ControlContexts
    {
        get { return contexts; }
        protected set
        {
            SetProperty(ref contexts, value, nameof(ControlContexts));
        }
    }

    public override event EventHandler<ContextChangeEventArgs> ContextChanged;

    public override ControlContext CurrentContext { get { return ControlContexts.Count > 0 ? ControlContexts[ControlContexts.Count - 1] : null; } }

    private Dictionary<System.Type, ControlContext> allContexts = new Dictionary<System.Type, ControlContext>();

    void Awake()
    {
        foreach (var context in transform.GetComponentsInChildren<ControlContext>(true))
        {
            allContexts[context.GetType()] = context;
        }
        if (startingView)
        {
            contexts.Add(startingView);
            startingView.Init(null)
                .ContinueWith(p =>
                {
                    p.Result.gameObject.SetActive(true);
                    return p;
                }, TaskContinuationOptions.ExecuteSynchronously);
        }
    }

    public override async Task PushContext<T>(object initArgs = null, bool deactivateCurrentControlObject = false)
    {
        await PushContext(typeof(T), initArgs, deactivateCurrentControlObject);
    }

    private async Task PushContext(Type type, object initArgs = null, bool deactivateCurrentControlObject = false)
    {
        ControlContext oldContext = CurrentContext;
        ControlContext context;
        allContexts.TryGetValue(type, out context);
        if (!context)
        {
            throw new Exception($"Unable to push context of type {type}. Context not found in stack hierarchy.");
        }
        if (contexts.Count - 1 >= 0)
        {
            contexts[contexts.Count - 1].enabled = false;
            if (deactivateCurrentControlObject)
            {
                contexts[contexts.Count - 1].gameObject.SetActive(false);
            }
        }
        contexts.Add(context);
        try
        {
            await context.Init(initArgs)
            .ContinueWith(p =>
            {
                p.Result.gameObject.SetActive(true);
                OnContextChanged(oldContext, p.Result);
                return p;
            }, TaskContinuationOptions.ExecuteSynchronously);
        }
        catch (Exception e)
        {
            Debug.LogError($"Error pushing context to stack: {e}");
            contexts.Remove(contexts.Count - 1);
            if (contexts.Count > 0)
            {
                contexts[contexts.Count - 1].gameObject.SetActive(true);
                contexts[contexts.Count - 1].enabled = true;
            }
        }
    }

    public override Task PopContext()
    {
        ControlContext oldContext = null;
        ControlContext newContext = null;
        if (contexts.Count <= 0)
        {
            Debug.LogWarning("Trying to pop a control context with none pushed.");
        }
        else
        {
            oldContext = CurrentContext;
            CurrentContext.gameObject.SetActive(false);
            contexts.RemoveAt(contexts.Count - 1);
            newContext = CurrentContext;
            CurrentContext.PopTo(); // Make this async and await it?
            CurrentContext.gameObject.SetActive(true);
            CurrentContext.enabled = true;
            OnContextChanged(oldContext, newContext);
        }
        return Task.CompletedTask;
    }

    public override void ClearContexts()
    {
        var oldContext = CurrentContext;
        foreach (var context in contexts)
        {
            context.gameObject.SetActive(false);
        }
        contexts.Clear();
    }

    private void OnContextChanged(ControlContext oldContext, ControlContext newContext)
    {
        OnPropertyChanged(nameof(CurrentContext));
        ContextChanged?.Invoke(this, new ContextChangeEventArgs(oldContext, newContext));
    }
}
