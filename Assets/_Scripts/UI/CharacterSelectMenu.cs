﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityWeld.Binding;
using Zenject;

[Binding]
internal class CharacterSelectMenu : ControlContext
{

    private UnityEvent _onCancel = new UnityEvent();
    public UnityEvent onCancel { get { return _onCancel; } }

    public CharacterSlot characterSlot { get; set; }

    private ObservableList<ButtonViewModel> characters = new ObservableList<ButtonViewModel>();
    [Binding]
    public ObservableList<ButtonViewModel> Characters
    {
        get { return characters; }
        private set { SetProperty(ref characters, value, "Characters"); }
    }

    [Inject]
    ICharacterSelectState characterSelectState;
    [Inject]
    ABoardManager boardManager;

    private void AddCharacter(ACharacter character)
    {
        // TODO: remove any character in current slot.
        RemoveCharacter(characterSlot.Character);
        characterSelectState.SelectCharacter(character, characterSlot);
        contextStack.PopContext();
    }

    private void RemoveCharacter(ACharacter character)
    {
        if (characterSelectState.IsCharacterSelected(character))
        {
            characterSelectState.DeselectCharacter(character);
        }
    }

    public override Task<ControlContext> Init(object initArgs)
    {
        return base.Init(initArgs)
        .ContinueWith(antecedent =>
        {
            characterSlot = initArgs as CharacterSlot;
            Characters.Clear();
            var first = true;
            characterSelectState.GetAllCharacters()
                .ForEach(c =>
                {
                    Characters.Add(
                        new ButtonViewModel
                        {
                            Text = c.Name,
                            Command = () =>
                            {
                                AddCharacter(c);
                            },
                            CancelCommand = () =>
                            {
                                contextStack.PopContext();
                            },
                            Selected = first
                        }
                    );
                    first = false;
                });
            Characters[0].Selected = true;
            return (ControlContext)this;
        },
        TaskContinuationOptions.ExecuteSynchronously);
    }

    public override void OnCancel(BaseEventData eventData)
    {
        contextStack.PopContext();
    }
}
