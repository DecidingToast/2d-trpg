﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

internal class CombatPreviewDisplay : ACursorListenerDisplay
{
    private AbilityCursorPanelView cursorView;
    [Binding]
    public AbilityCursorPanelView CursorView
    {
        get { return cursorView; }
        private set { SetProperty(ref cursorView, value, nameof(CursorView)); }
    }

    private bool showing;
    [Binding]
    public bool Showing
    {
        get { return Attacker != null && Defender != null && CursorView.Ability.GetType() == typeof(Attack); }
    }

    private ACharacter attacker;
    [Binding]
    public ACharacter Attacker
    {
        get { return attacker; }
        private set { SetProperty(ref attacker, value, nameof(Attacker)); }
    }

    private ACharacter defender;
    [Binding]
    public ACharacter Defender
    {
        get { return defender; }
        private set { SetProperty(ref defender, value, nameof(Defender)); }
    }

    private int attackerHp;
    [Binding]
    public int AttackerHP
    {
        get { return attackerHp; }
        set { SetProperty(ref attackerHp, value, nameof(AttackerHP)); }
    }

    private int defenderHp;
    [Binding]
    public int DefenderHP
    {
        get { return defenderHp; }
        set { SetProperty(ref defenderHp, value, nameof(DefenderHP)); }
    }

    private float attackerHit;
    [Binding]
    public float AttackerHit
    {
        get { return attackerHit; }
        set { SetProperty(ref attackerHit, value, nameof(AttackerHit)); }
    }

    private float defenderHit;
    [Binding]
    public float DefenderHit
    {
        get { return defenderHit; }
        set { SetProperty(ref defenderHit, value, nameof(DefenderHit)); }
    }

    private float attackerCrit;
    [Binding]
    public float AttackerCrit
    {
        get { return attackerCrit; }
        set { SetProperty(ref attackerCrit, value, nameof(AttackerCrit)); }
    }

    private float defenderCrit;
    [Binding]
    public float DefenderCrit
    {
        get { return defenderCrit; }
        set { SetProperty(ref defenderCrit, value, nameof(DefenderCrit)); }
    }

    private int attackerNumAttacks;
    [Binding]
    public int AttackerNumAttacks
    {
        get { return attackerNumAttacks; }
        set { SetProperty(ref attackerNumAttacks, value, nameof(AttackerNumAttacks)); }
    }

    private int defenderNumAttacks;
    [Binding]
    public int DefenderNumAttacks
    {
        get { return defenderNumAttacks; }
        set { SetProperty(ref defenderNumAttacks, value, nameof(DefenderNumAttacks)); }
    }

    protected override void OnHoverCharacter(ACharacter c)
    {
        Defender = c;
        OnPropertyChanged(nameof(Showing));
    }

    protected override void CursorChanged(CursorControllerPanelView cursor)
    {
        var cView = cursor as AbilityCursorPanelView;
        if (cView != null)
        {
            CursorView = cView;
            Attacker = cView.Ability.GetComponent<ACharacter>();
            OnPropertyChanged(nameof(Showing));
        }
    }
}
