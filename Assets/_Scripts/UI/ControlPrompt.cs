﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal class ControlPrompt : MonoBehaviour
{
    [SerializeField]
    private string inputString;
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetButton(inputString))
        {
            animator.SetBool("ShowControls", true);
        }
        else
        {
            animator.SetBool("ShowControls", false);
        }
    }
}
