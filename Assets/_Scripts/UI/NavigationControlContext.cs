﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// A control context that selects a Selectable UI element on enable
/// </summary>
internal class NavigationControlContext : ControlContext
{
    [SerializeField]
    protected Selectable defaultSelected;

    private Selectable currentSelected;

    protected Selectable CurrentSelected
    {
        get
        {
            if (!currentSelected)
            {
                currentSelected = defaultSelected;
            }
            return currentSelected;
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (CurrentSelected != null && CurrentSelected != EventSystem.current.currentSelectedGameObject)
        {
            CurrentSelected.StartCoroutine(WaitOneFrameThenSelect());
        }
    }

    private IEnumerator WaitOneFrameThenSelect()
    {
        yield return null;
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(CurrentSelected.gameObject);
        yield return null;
        Debug.Log("Selecting button");
        CurrentSelected.Select();
    }
}
