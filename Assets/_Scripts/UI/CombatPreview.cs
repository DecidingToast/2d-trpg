﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;
using Pathfinding;
using Zenject;
using System;
using UnityEngine.EventSystems;
using System.Threading.Tasks;

internal class CombatPreview : ControlContext
{
    const int DOUBLING_THRESHOLD = 5;
    const int CRIT_MULTIPLIER = 3;

    private List<ACharacter> targets;

    #region BINDINGS
    private ACharacter attacker;
    [Binding]
    public ACharacter Attacker
    {
        get { return attacker; }
        set
        {
            if (SetProperty(ref attacker, value, nameof(Attacker)))
            {
                UpdatePreview();
            }
        }
    }

    private ACharacter defender;
    [Binding]
    public ACharacter Defender
    {
        get { return defender; }
        set
        {
            if (SetProperty(ref defender, value, nameof(Defender)))
            {
                UpdatePreview();
            }
        }
    }

    private int attackerDamage;
    [Binding]
    public int AttackerDamage
    {
        get { return attackerDamage; }
        set { SetProperty(ref attackerDamage, value, nameof(AttackerDamage)); }
    }

    private int defenderDamage;
    [Binding]
    public int DefenderDamage
    {
        get { return defenderDamage; }
        set { SetProperty(ref defenderDamage, value, nameof(DefenderDamage)); }
    }

    private float attackerHit;
    [Binding]
    public float AttackerHit
    {
        get { return attackerHit; }
        set { SetProperty(ref attackerHit, value, nameof(AttackerHit)); }
    }

    private float defenderHit;
    [Binding]
    public float DefenderHit
    {
        get { return defenderHit; }
        set { SetProperty(ref defenderHit, value, nameof(DefenderHit)); }
    }

    private float attackerCrit;
    [Binding]
    public float AttackerCrit
    {
        get { return attackerCrit; }
        set { SetProperty(ref attackerCrit, value, nameof(AttackerCrit)); }
    }

    private float defenderCrit;
    [Binding]
    public float DefenderCrit
    {
        get { return defenderCrit; }
        set { SetProperty(ref defenderCrit, value, nameof(DefenderCrit)); }
    }

    private bool attackerDouble;
    [Binding]
    public bool AttackerDouble
    {
        get { return attackerDouble; }
        set { SetProperty(ref attackerDouble, value, nameof(AttackerDouble)); }
    }
    #endregion

    [Inject]
    ABoardManager boardManager;

    public override async Task<ControlContext> Init(object initArgs)
    {
        var ret = base.Init(initArgs);
        Attacker = initArgs as ACharacter;
        targets = boardManager.GetAllCharacters(Team.ENEMY);
        Defender = targets[0];
        return await ret;
    }

    public override void OnSubmit(BaseEventData eventData)
    {

    }

    public override void OnCancel(BaseEventData eventData)
    {

    }

    private void UpdatePreview()
    {
        //TODO: Move constants to bundled reference data
        var attackerAcc = attacker.CurrSkl * 2 + attacker.Weapon.Acc;
        var defenderAcc = defender.CurrSkl * 2 + defender.Weapon.Acc;
        var attackerAvo = attacker.CurrSpd * 2;
        var defenderAvo = defender.CurrSpd * 2;
        var attackerCritChance = (float)attacker.CurrSkl / 2f + attacker.Weapon.Crit;
        var defenderCritChance = (float)defender.CurrSkl / 2f + defender.Weapon.Crit;
        var attackerCritAvo = (float)attacker.CurrSkl / 2f;
        var defenderCritAvo = (float)defender.CurrSkl / 2f;

        AttackerCrit = Mathf.Clamp01(attackerCritChance - attackerCritAvo);
        DefenderCrit = Mathf.Clamp01(defenderCritChance - defenderCritAvo);
        AttackerHit = Mathf.Clamp01(attackerAcc - defenderAvo);
        DefenderHit = Mathf.Clamp01(defenderAcc - attackerAvo);
        AttackerDamage = DamageCalc(attacker, defender);
        DefenderDamage = DamageCalc(defender, attacker);
        AttackerDouble = attacker.CurrSpd - DOUBLING_THRESHOLD >= defender.CurrSpd;
    }

    private int DamageCalc(ACharacter a, ACharacter b)
    {
        int ret = 0;
        switch (a.Weapon.DamType)
        {
            case Weapon.DamageType.PHYSICAL:
                ret = a.CurrStr + a.Weapon.Mt - b.CurrDef;
                break;
            case Weapon.DamageType.MAGIC:
                ret = a.CurrMag + a.Weapon.Mt - b.CurrRes;
                break;
        }
        return Math.Max(0, ret);
    }
}
