using UnityEngine;
using UnityEngine.UI;
using Zenject;

[RequireComponent(typeof(Image))]
internal class GetImage : MonoBehaviour
{
    [Inject]
    IAssetLocator<Sprite> spriteLocator;

    private string spriteName;
    public string SpriteName
    {
        get { return spriteName; }
        set
        {
            spriteName = value;
            if (!string.IsNullOrEmpty(spriteName))
            {
                var sprite = spriteLocator.GetAsset(spriteName);
                GetComponent<Image>().sprite = sprite;
            }
        }
    }

}