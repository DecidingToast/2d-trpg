using UnityEngine.EventSystems;

internal interface IButtonHandler : IEventSystemHandler
{
    void OnButtonPressed(BaseEventData eventData);
}