﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityWeld.Binding;
using Zenject;

[Binding]
internal abstract class ControlContext : DataBindMonobehaviour, ICancelHandler, ISubmitHandler
{
    [Inject]
    protected AControlStack contextStack;


    /// <summary>
    /// Call base after all initialization completes.
    /// </summary>
    /// <param name="initArgs"></param>
    /// <returns></returns>
    public virtual Task<ControlContext> Init(object initArgs)
    {
        return Task.FromResult(this);
    }

    /// <summary>
    /// Called when PanelStack.popPanel returns control to this panel.
    /// </summary>
    public virtual void PopTo() { }

    public virtual void OnCancel(BaseEventData eventData) { }

    public virtual void OnSubmit(BaseEventData eventData) { }

    protected virtual void OnEnable()
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
    }
}
