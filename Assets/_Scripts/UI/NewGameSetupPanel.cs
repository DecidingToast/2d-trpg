using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityWeld.Binding;
using Zenject;

[Binding]
internal class NewGameSetupPanel : DataBindMonobehaviour// : ControlContext
{

    [Inject]
    private IRefDataProvider refDataProvider;
    private RefDataTable<CharacterClass> classTable;
    private RefDataTable<StatSpread> statTable;

    [SerializeField]
    private CharacterStats[] starterCharacters;
    [Binding]
    public CharacterStats[] StarterCharacters
    {
        get { return starterCharacters; }
        private set { SetProperty(ref starterCharacters, value, nameof(StarterCharacters)); }
    }

    [Binding]
    public CharacterStats CurrentCharacter => StarterCharacters[ClassIndex];

    private int classIndex;
    [Binding]
    public int ClassIndex
    {
        get { return classIndex; }
        private set
        {
            if (SetProperty(ref classIndex, value, nameof(ClassIndex)))
            {
                OnPropertyChanged(nameof(CurrentCharacter));
            }
        }
    }

    private bool initialized = false;

    private async void Start()
    {
        try
        {
            var classTableTask = refDataProvider.GetTableAsync<CharacterClass>();
            var statTableTask = refDataProvider.GetTableAsync<StatSpread>();

            await Task.WhenAll(classTableTask, statTableTask);

            classTable = classTableTask.Result;
            statTable = statTableTask.Result;

            initialized = true;
        }
        catch (Exception e)
        {
            Debug.LogError($"Error setting up CharacterCreationPanel: {e}");
        }
    }
}