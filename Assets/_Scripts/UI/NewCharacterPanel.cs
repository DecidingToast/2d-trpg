﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using UnityEngine;
using UnityWeld.Binding;
using Zenject;

/// <summary>
/// Hooo boooyyy, lot of string logic here. Meh.
/// </summary>
[Binding]
public class NewCharacterPanel : DataBindMonobehaviour
{
    private static string[] StatNames =
    {
        "HP",
        "Str",
        "Mag",
        "Def",
        "Res",
        "Skl",
        "Spd",
    };

    private Dictionary<string, (int, int)> customPlayerStatBaseModifications = new Dictionary<string, (int, int)>
    {
        { "HP", (5, -3) },
        { "Str", (2, -1) },
        { "Mag", (2, -1) },
        { "Def", (2, -1) },
        { "Res", (2, -1) },
        { "Skl", (2, -1) },
        { "Spd", (2, -1) },
    };

    private string GrowthRatesName => $"characterRates_Asset_{CurrentAsset}_Flaw_{CurrentFlaw}";
    private string StatCapsName => $"characterCaps_Asset_{CurrentAsset}_Flaw_{CurrentFlaw}";

    [SerializeField]
    private CharacterClass[] starterClasses;

    #region ARRAY_INDICES
    private int classIndex;
    [Binding]
    public int ClassIndex
    {
        get { return classIndex; }
        set
        {
            var old = classIndex;
            if (SetProperty(ref classIndex, value, nameof(ClassIndex)))
            {
                OnPropertyChanged(nameof(CurrentClass));
                ChangeClass(starterClasses[old], starterClasses[value]);
            }
        }
    }

    private int assetIndex;
    [Binding]
    public int AssetIndex
    {
        get { return assetIndex; }
        set
        {
            var old = assetIndex;
            if (SetProperty(ref assetIndex, value, nameof(AssetIndex)))
            {
                OnPropertyChanged(nameof(CurrentAsset));
                ChangeAsset(StatNames[old], StatNames[value]);
                CharacterStats.GrowthRates = GrowthRatesName;
                CharacterStats.StatCaps = StatCapsName;
            }
        }
    }

    private int flawIndex;
    [Binding]
    public int FlawIndex
    {
        get { return flawIndex; }
        set
        {
            var old = flawIndex;
            if (SetProperty(ref flawIndex, value, nameof(FlawIndex)))
            {
                OnPropertyChanged(nameof(CurrentFlaw));
                ChangeFlaw(StatNames[old], StatNames[value]);
                CharacterStats.GrowthRates = GrowthRatesName;
                CharacterStats.StatCaps = StatCapsName;
            }
        }
    }
    #endregion

    #region STAT_TOTALS

    private int hp;
    [Binding]
    public int HP
    {
        get { return hp; }
        set { SetProperty(ref hp, value, nameof(HP)); }
    }

    private int str;
    [Binding]
    public int Str
    {
        get { return str; }
        set { SetProperty(ref str, value, nameof(Str)); }
    }

    private int mag;
    [Binding]
    public int Mag
    {
        get { return mag; }
        set { SetProperty(ref mag, value, nameof(Mag)); }
    }

    private int def;
    [Binding]
    public int Def
    {
        get { return def; }
        set { SetProperty(ref def, value, nameof(Def)); }
    }

    private int res;
    [Binding]
    public int Res
    {
        get { return res; }
        set { SetProperty(ref res, value, nameof(Res)); }
    }

    private int skl;
    [Binding]
    public int Skl
    {
        get { return skl; }
        set { SetProperty(ref skl, value, nameof(Skl)); }
    }

    private int spd;
    [Binding]
    public int Spd
    {
        get { return spd; }
        set { SetProperty(ref spd, value, nameof(Spd)); }
    }

    private int mov;
    [Binding]
    public int Mov
    {
        get { return mov; }
        set { SetProperty(ref mov, value, nameof(Mov)); }
    }

    #endregion

    [Binding]
    public CharacterClass CurrentClass => starterClasses[classIndex];
    [Binding]
    public string CurrentAsset => StatNames[assetIndex];
    [Binding]
    public string CurrentFlaw => StatNames[flawIndex];

    [SerializeField]
    private CharacterStats characterStats;
    [Binding]
    public CharacterStats CharacterStats
    {
        get { return characterStats; }
        set { SetProperty(ref characterStats, value, nameof(CharacterStats)); }
    }

    [SerializeField]
    private string charName = "";
    [Binding]
    public string CharName
    {
        get { return charName; }
        set
        {
            if (SetProperty(ref charName, value, nameof(CharName)))
            {
                CharacterStats.Name = value;
            }
        }
    }

    private void Start()
    {
        HP = characterStats.HP;
        Str = characterStats.Str;
        Mag = characterStats.Mag;
        Def = characterStats.Def;
        Res = characterStats.Res;
        Skl = characterStats.Skl;
        Spd = characterStats.Spd;
        Mov = characterStats.Mov;
        ChangeClass(null, CurrentClass);
        ChangeAsset(null, CurrentAsset);
        ChangeFlaw(null, CurrentFlaw);
    }

    private void ChangeClass(CharacterClass oldClass, CharacterClass newClass)
    {
        var oldBases = oldClass?.BaseStats;
        var newBases = newClass?.BaseStats;
        if (oldBases != null)
        {
            HP -= oldBases.HP;
            Str -= oldBases.Str;
            Mag -= oldBases.Mag;
            Def -= oldBases.Def;
            Res -= oldBases.Res;
            Skl -= oldBases.Skl;
            Spd -= oldBases.Spd;
        }
        if (newBases != null)
        {
            CharacterStats.HP = HP += newBases.HP;
            CharacterStats.Str = Str += newBases.Str;
            CharacterStats.Mag = Mag += newBases.Mag;
            CharacterStats.Def = Def += newBases.Def;
            CharacterStats.Res = Res += newBases.Res;
            CharacterStats.Skl = Skl += newBases.Skl;
            CharacterStats.Spd = Spd += newBases.Spd;

            CharacterStats.Mov = Mov = newBases.Mov;
        }
    }

    private void ChangeAsset(string oldAsset, string newAsset)
    {
        if (oldAsset != null)
        {
            ChangeStat(oldAsset, -customPlayerStatBaseModifications[oldAsset].Item1);
        }
        if (newAsset != null)
        {
            ChangeStat(newAsset, customPlayerStatBaseModifications[newAsset].Item1);
        }
    }

    private void ChangeFlaw(string oldFlaw, string newFlaw)
    {
        if (oldFlaw != null)
        {
            ChangeStat(oldFlaw, -customPlayerStatBaseModifications[oldFlaw].Item2);
        }
        if (newFlaw != null)
        {
            ChangeStat(newFlaw, customPlayerStatBaseModifications[newFlaw].Item2);
        }
    }

    private void ChangeStat(string stat, int value)
    {
        switch (stat)
        {
            case "HP":
                HP += value;
                break;
            case "Str":
                Str += value;
                break;
            case "Mag":
                Mag += value;
                break;
            case "Def":
                Def += value;
                break;
            case "Res":
                Res += value;
                break;
            case "Skl":
                Skl += value;
                break;
            case "Spd":
                Spd += value;
                break;
        }
    }
}
