﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pathfinding;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

internal class AbilityCursorPanelView : CursorControllerPanelView
{
    private Ability ability;
    public Ability Ability
    {
        get { return ability; }
        set
        {
            SetProperty(ref ability, value, "Ability");
        }
    }

    private GraphNode prevNode;

    [Inject]
    private AObjectPool<GameObject> prefabPool;

    protected GameObject[] nodeDisplay;

    public override async Task<ControlContext> Init(object initArgs)
    {
        Ability = initArgs as Ability;
        return await base.Init(initArgs);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        prevNode = cursor.Node = Graph.GetNearest(Ability.transform.position).node;
        cursor.transform.position = (Vector3)cursor.Node.position;
        ShowRange(cursor.Node as GridNodeBase, Ability.MinRange, Ability.MaxRange);
    }

    public override void OnSubmit(BaseEventData eventData)
    {
        if (Ability.CanActivate())
        {
            var target = cursor.Node;
            if (Ability.ValidTarget(target))
            {
                HideRange();
                Ability.ActivateAsync(target)
                .ContinueWith(antecendent =>
                {
                    Node = boardManager.GetNodeFromCharacter(ability.GetComponent<ACharacter>()) as GridNodeBase;
                    cursor.transform.position = (Vector3)Node.position;
                    contextStack.ClearContexts();
                    contextStack.PushContext<MoveCharacterPanelView>();
                }, TaskContinuationOptions.ExecuteSynchronously);
            }
        }
    }

    public override void OnCancel(BaseEventData eventData)
    {
        CancelMoveCursor();
        cursor.transform.position = (Vector3)prevNode.position;
        cursor.Node = prevNode;
        HideRange();
        contextStack.PopContext();
    }

    private void ShowRange(GridNodeBase origin, int min, int max)
    {
        HideRange();
        var nodes = Graph.nodes
            .Where(node =>
            {
                var dist = GridGraphUtils.Distance(Graph, origin, node);
                return dist >= min && dist <= max && ShowNodePredicate(node);
            });
        nodeDisplay = prefabPool.Retrieve(nodes.Count());
        int i = 0;
        foreach (var node in nodes)
        {
            nodeDisplay[i].transform.position = (Vector3)node.position;
            nodeDisplay[i].SetActive(true);
            ++i;
        }
    }

    private void HideRange()
    {
        if (nodeDisplay != null)
        {
            prefabPool.Release(nodeDisplay);
            nodeDisplay = null;
        }
    }
}
