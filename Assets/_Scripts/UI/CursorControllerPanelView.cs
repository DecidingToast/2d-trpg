using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pathfinding;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Zenject;

internal abstract class CursorControllerPanelView : ControlContext
{
    private const float CURSOR_SPEED = .25f;
    private const float TIME_TO_MOVE = .1f;

    public event Action<ACharacter> OnHoverCharacter;

    protected GridGraph Graph { get { return AstarData.active.data.gridGraph; } }

    private GridNodeBase node;
    protected GridNodeBase Node
    {
        get { return node; }
        set
        {
            node = value;
            cursor.Node = node;
            OnHoverCharacter?.Invoke(boardManager.GetCharacterFromNode(node));
        }
    }

    protected bool moving = false;

    [SerializeField]
    protected GameObject startPosition;

    [SerializeField]
    protected GridNodeHolder cursor;

    [Inject]
    protected ABoardManager boardManager;

    protected override void OnEnable()
    {
        base.OnEnable();
        cursor.enabled = true;
        node = cursor.Node as GridNodeBase;
    }

    protected virtual void OnDisable()
    {
        cursor.enabled = false;
    }

    private bool snapped = false;

    protected virtual void FixedUpdate()
    {
        var cameraForward = new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z).normalized;

        var lowerLeft = (Vector3)Graph?.GetNode(0, 0).position;
        var upperRight = (Vector3)Graph?.GetNode(Graph.Width - 1, Graph.Depth - 1)?.position;

        var cameraVector = Quaternion.FromToRotation(Vector3.forward, cameraForward) * new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));

        var xAxis = cameraVector.x;
        var zAxis = cameraVector.z;
        var currX = cursor.transform.position.x;
        var currZ = cursor.transform.position.z;

        xAxis = (currX <= lowerLeft.x && xAxis < 0) || (currX >= upperRight.x && xAxis > 0) ? 0f : xAxis;
        zAxis = (currZ <= lowerLeft.z && zAxis < 0) || (currZ >= upperRight.z && zAxis > 0) ? 0f : zAxis;

        if (xAxis != 0f || zAxis != 0f)
        {
            CancelMoveCursor();
            cursor.transform.position += new Vector3(xAxis, 0, zAxis) * CURSOR_SPEED;
            Node = Graph.GetNearest(cursor.transform.position).node as GridNodeBase;
            snapped = false;
        }
        else
        {
            if (!snapped)
            {
                MoveCursor(Node);
            }

        }
    }

    protected virtual void Update()
    {
        handleInput();
    }

    /// <summary>
    /// Handles any input this cursor must respond to.
    /// </summary>
    protected virtual void handleInput() { }

    private int? tween;
    protected virtual void MoveCursor(GridNodeBase newNode)
    {
        if (newNode != null)
        {
            CancelMoveCursor();
            tween = LeanTween.move(cursor.gameObject, ((Vector3)newNode.position), TIME_TO_MOVE)
            .setOnComplete(() =>
            {
                Node = newNode;
            })
            .id;
        }
    }

    protected void CancelMoveCursor()
    {
        if (tween.HasValue)
        {
            LeanTween.cancel(tween.Value);
        }
        tween = null;
    }

    protected virtual bool ShowNodePredicate(GridNodeBase node)
    {
        return true;
    }

    public abstract override void OnSubmit(BaseEventData eventData);
}
