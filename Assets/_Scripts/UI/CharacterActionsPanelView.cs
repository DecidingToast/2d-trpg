using System;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityWeld.Binding;
using Zenject;

[Binding]
internal class CharacterActionsPanelView : ControlContext
{

    [Inject]
    private ABoardManager boardManager;

    private ObservableList<ButtonViewModel> actions = new ObservableList<ButtonViewModel>();
    [Binding]
    public ObservableList<ButtonViewModel> Actions
    {
        get { return actions; }
        private set { SetProperty(ref actions, value, "Actions"); }
    }

    private ACharacter character;
    [Binding]
    public ACharacter Character
    {
        get { return character; }
        set
        {
            if (SetProperty(ref character, value, "Character"))
            {
                actions.Clear();
            }
            var first = true;
            boardManager.GetAllInteractables()
                .ForEach(i =>
                {
                    if (i.CharacterCanInteract(Character))
                    {
                        actions.Add(new ButtonViewModel
                        {
                            Command = () =>
                            {
                                //TODO: Make dummy panelview for interactable behavior (maybe)
                                contextStack.ClearContexts();
                                i.InteractAsync(Character)
                                    .ContinueWith(antecedent =>
                                    {
                                        contextStack.PushContext<MoveCharacterPanelView>();
                                    }, TaskContinuationOptions.ExecuteSynchronously);
                            },
                            CancelCommand = () =>
                            {
                                contextStack.PopContext();
                            },
                            Text = i.Name,
                            Selected = first
                        });
                        first = false;
                    }
                });
            Character.GetComponents<Ability>()
                .ToList()
                .ForEach(a =>
                {
                    if (a.CanActivate())
                    {
                        actions.Add(new ButtonViewModel
                        {
                            Command = () =>
                            {
                                //TODO: Make dummy panelview for activity behavior
                                contextStack.PushContext<AbilityCursorPanelView>(a);
                            },
                            CancelCommand = () =>
                            {
                                contextStack.PopContext();
                            },
                            Text = a.Name,
                            Selected = first
                        });

                        first = false;
                    }
                });
            actions.Add(new ButtonViewModel
            {
                Command = () =>
                {
                    // Wait, take no action.
                    Character.HasMoved = true;
                    contextStack.ClearContexts();
                    contextStack.PushContext<MoveCharacterPanelView>();

                },
                CancelCommand = () =>
                {
                    contextStack.PopContext();
                },
                Text = "Wait",
                Selected = first
            });
            first = false;

            var currNode = boardManager.GetNodeFromCharacter(Character);
            actions.Add(new ButtonViewModel
            {
                Command = () =>
                {
                    // Cancel, move character back to original position.
                    Character.TeleportTo(currNode);
                    contextStack.PopContext();
                },
                CancelCommand = () =>
                {
                    contextStack.PopContext();
                },
                Text = "Cancel",
                Selected = first
            });
        }
    }

    public override async Task<ControlContext> Init(object initArgs)
    {
        actions.Clear();
        var chara = initArgs as ACharacter;
        if (!chara)
        {
            throw new Exception("No character passed to initialization.");
        }
        else
        {
            Character = initArgs as ACharacter;
            return await base.Init(initArgs);
        }
    }

    public override void OnCancel(BaseEventData eventData)
    {
        contextStack.PopContext();
    }
}