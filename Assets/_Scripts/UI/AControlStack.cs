using System;
using UnityWeld.Binding;
using System.Threading.Tasks;
using UnityEngine;

internal abstract class AControlStack : DataBindMonobehaviour
{
    internal class ContextChangeEventArgs : EventArgs
    {
        public ControlContext OldContext { get; private set; }
        public ControlContext NewContext { get; private set; }

        private ControlContext interceptContext;
        /// <summary>
        /// Receivers should set this if they want to intercept a context change.
        /// NewContext will be queued after the intercepting context.
        /// The intercepting context will not determine where it exits to (it will always transfer control to the intercepted context).
        /// If it is already set, subsequent sets will not change it.
        /// </summary>
        public ControlContext InterceptContext
        {
            get { return interceptContext; }
            set
            {
                if (interceptContext == null)
                {
                    interceptContext = value;
                }
                else
                {
                    Debug.LogWarning($"Intercept context was already set {interceptContext.GetType()}. It cannot be set again to {value.GetType()} for this context change.");
                }
            }
        }

        public ContextChangeEventArgs(ControlContext oldContext, ControlContext newContext)
        {
            OldContext = oldContext;
            NewContext = newContext;
        }
    }

    public abstract ObservableList<ControlContext> ControlContexts { get; protected set; }
    public abstract ControlContext CurrentContext { get; }
    public abstract event EventHandler<ContextChangeEventArgs> ContextChanged;
    public abstract Task PushContext<T>(object contextInitArgs = null, bool deactivateCurrentControlObject = false) where T : ControlContext;
    public abstract Task PopContext();
    public abstract void ClearContexts();
}