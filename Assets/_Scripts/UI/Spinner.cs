﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// A Spinner control for use with gamepad input.
/// </summary>
public class Spinner : Selectable
{
    [Serializable]
    public class SpinnerValueChangedEvent : UnityEvent { }

    private int index;
    public int Index
    {
        get { return index; }
        set { index = value; }
    }


    [SerializeField]
    int max = 10;

    [SerializeField]
    private string[] leftButtonNames;
    [SerializeField]
    private string[] rightButtonNames;

    [SerializeField]
    private float waitTime = .1f;
    private float currWaited = 0f;

    [SerializeField]
    public SpinnerValueChangedEvent ValueChanged = new SpinnerValueChangedEvent();

    void Update()
    {
        currWaited += Time.deltaTime;
        if (EventSystem.current?.currentSelectedGameObject == gameObject)
        {
            if (!LeftPressed() && !RightPressed())
            {
                currWaited = 1f;
            }
            if (currWaited > waitTime)
            {
                var old = Index;
                if (RightPressed())
                {
                    Index = (Index + 1) % max;
                }
                if (LeftPressed())
                {
                    Index = (max + Index - 1) % max;
                }
                if (old != Index)
                {
                    ValueChanged?.Invoke();
                }
                currWaited = 0f;
            }
        }
    }

    private bool LeftPressed()
    {
        return Input.GetAxis("Horizontal") < 0 || leftButtonNames.Any(name => Input.GetButtonDown(name));
    }

    private bool RightPressed()
    {
        return Input.GetAxis("Horizontal") > 0 || rightButtonNames.Any(name => Input.GetButtonDown(name));
    }
}
