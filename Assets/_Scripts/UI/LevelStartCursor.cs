﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pathfinding;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityWeld.Binding;
using Zenject;

internal class LevelStartCursor : CursorControllerPanelView
{
    [Inject]
    private ICharacterSelectState _characterSelectState;
    [Inject]
    private ABoardManager _boardManager;

    protected void Start()
    {
        if (!startPosition)
            startPosition = gameObject;

        if (Graph == null)
        {
            enabled = false;
            Debug.Log("No grid graph available, aborting");
            return;
        }
        Node = Graph.GetNearest(startPosition.transform.position).node as GridNodeBase;
        if (Node == null)
        {
            Node = Graph.GetNearest(transform.position).node as GridNodeBase;
        }
        cursor.transform.position = ((Vector3)Node.position);
        cursor.Node = Node;
    }

    protected override void handleInput()
    {
        if (Input.GetButtonDown("Start"))
        {
            if (_characterSelectState.SelectedCount > 0)
            {
                _characterSelectState.Slots.ForEach(s => s.gameObject.SetActive(false));
                contextStack.ClearContexts();
                contextStack.PushContext<MoveCharacterPanelView>(null);
            }
        }
    }

    public override void OnSubmit(BaseEventData eventData)
    {
        var slot = _characterSelectState.Slots
                        .Where(s => Graph.GetNearest(s.transform.position).node == Graph.GetNearest(cursor.transform.position).node)
                        .FirstOrDefault();
        if (slot)
            contextStack.PushContext<CharacterSelectMenu>(slot);
    }

    public override void OnCancel(BaseEventData eventData)
    {
    }
}
