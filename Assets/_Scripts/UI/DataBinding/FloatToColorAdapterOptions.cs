﻿using UnityEngine;
using UnityWeld.Binding;

[CreateAssetMenu(menuName = "Unity Weld/Adapter options/Custom/Float to Color adapter options")]
internal class FloatToColorAdapterOptions : AdapterOptions
{
    public int MinValue;
    public Color MinColor;

    public int MaxValue;
    public Color MaxColor;
}
