﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

[Binding]
internal class ButtonViewModel : DataBindObject
{
    private Action command;
    [Binding]
    public Action Command
    {
        get { return command; }
        set
        {
            SetProperty(ref command, value, "Command");
        }
    }

    private Action cancelCommand;
    [Binding]
    public Action CancelCommand
    {
        get { return cancelCommand; }
        set
        {
            SetProperty(ref cancelCommand, value, "CancelCommand");
        }
    }

    private string text;
    [Binding]
    public string Text
    {
        get { return text; }
        set
        {
            SetProperty(ref text, value, "Text");
        }
    }

    private string icon;
    [Binding]
    public string Icon
    {
        get { return icon; }
        set
        {
            SetProperty(ref icon, value, "Icon");
        }
    }

    private bool selected = false;
    [Binding]
    public bool Selected
    {
        get { return selected; }
        set
        {
            SetProperty(ref selected, value, "Selected");
        }
    }

    [Binding]
    public void Execute()
    {
        if (Command != null)
            Command();
    }
}
