﻿using UnityEngine;
using UnityWeld.Binding;

[Adapter(typeof(int), typeof(Color), typeof(FloatToColorAdapterOptions))]
internal class FloatToColorAdapter : IAdapter
{
    public object Convert(object valueIn, AdapterOptions options)
    {
        float? value = valueIn as float?;
        var opts = options as FloatToColorAdapterOptions;
        if (opts != null && value != null)
        {
            float val = value.Value;
            float normalized = (val - opts.MinValue) / (opts.MinValue - opts.MinValue);
            return Color.Lerp(opts.MinColor,
                              opts.MaxColor,
                              normalized);

        }
        else
        {
            return Color.white;
        }
    }
}
