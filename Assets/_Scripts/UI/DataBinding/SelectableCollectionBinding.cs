using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityWeld.Binding;
using Zenject;

/// <summary>
/// Allows selectable UI elements to behave properly as elements in a collection binding
/// </summary>
internal class SelectableCollectionBinding : CollectionBinding
{
    [Inject]
    MonoBehaviourHost behaviourHost;

    public enum Direction
    {
        X, Y
    }

    private void OnEnable()
    {
        StartCoroutine(WaitThenUpdateNavigation());
    }

    public Direction direction;

    protected override void Collection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
        base.Collection_CollectionChanged(sender, e);
        if (enabled && gameObject.activeInHierarchy)
        {
            StartCoroutine(WaitThenUpdateNavigation());
        }
    }

    private IEnumerator WaitThenUpdateNavigation()
    {
        yield return null;
        var selectables = TemplatesRoot.GetComponentsInChildren<Selectable>()
            .OrderByDescending(selectable =>
            {
                switch (direction)
                {
                    case Direction.X: return selectable.transform.position.x;
                    case Direction.Y: return selectable.transform.position.y;
                    default: return 0; // Technically unreachable, but required to compile
                }
            })
            .ToList();

        for (int i = 0; i < selectables.Count; i++)
        {
            var s = selectables[i];
            var next = selectables[(i + 1) % selectables.Count];
            var prev = selectables[(selectables.Count + i - 1) % selectables.Count];
            switch (direction)
            {
                case Direction.X: s.navigation = new Navigation { mode = Navigation.Mode.Explicit, selectOnLeft = prev, selectOnRight = next }; break;
                case Direction.Y: s.navigation = new Navigation { mode = Navigation.Mode.Explicit, selectOnUp = prev, selectOnDown = next }; break;
            }
        }
    }
}