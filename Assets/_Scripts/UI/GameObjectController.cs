﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityWeld.Binding;

internal class GameObjectController : DataBindMonobehaviour
{
    [SerializeField]
    GameObject[] targets;

    public bool TurnOn
    {
        get { return targets.All(o => o.activeSelf); }
        set
        {
            foreach (var target in targets)
            {
                target.SetActive(TurnOn);
            }
        }
    }
}
