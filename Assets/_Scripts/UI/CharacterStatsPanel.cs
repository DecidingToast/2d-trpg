﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityWeld.Binding;
using Zenject;

/// <summary>
/// Should be placed on the top-level game object of the card
/// </summary>
[Binding]
[RequireComponent(typeof(Animator))]
internal class CharacterStatsPanel : ACursorListenerDisplay
{
    [SerializeField]
    private ACharacter _character;
    [Binding]
    public ACharacter Chara
    {
        get
        {
            return _character;
        }
        set
        {
            if (SetProperty(ref _character, value, nameof(Chara)))
            {
                Abilities.Clear();
                if (_character != null)
                {
                    foreach (var a in Chara.GetComponents<Ability>())
                    {
                        Abilities.Add(a);
                    }
                }
            }
        }
    }


    private ObservableList<Ability> abilities = new ObservableList<Ability>();
    [Binding]
    public ObservableList<Ability> Abilities
    {
        get { return abilities; }
        set { SetProperty(ref abilities, value, nameof(Abilities)); }
    }

    private bool showDetails = false;
    [Binding]
    public bool ShowDetails
    {
        get { return showDetails; }
        set
        {
            SetProperty(ref showDetails, value, nameof(ShowDetails));
        }
    }
    private bool show = false;
    [Binding]
    public bool Show
    {
        get { return show; }
        set
        {
            SetProperty(ref show, value, nameof(Show));
        }
    }

    void Update()
    {
        if (Input.GetButtonDown("Y") && Show)
        {
            ShowDetails = !ShowDetails;
        }
    }

    protected override void OnHoverCharacter(ACharacter c)
    {
        if (c != null)
        {
            Chara = c;
            Show = true;
        }
        else
        {
            Show = false;
            ShowDetails = false;
        }
        if (showDetails)
        {
            ShowDetails = c;
        }
    }
}
