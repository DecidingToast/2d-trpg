using Pathfinding;
using UnityEngine;
using UnityWeld.Binding;
using Zenject;

[Binding]
internal class GridNodeHolder : DataBindMonobehaviour
{
    //[Inject]
    private ABoardManager boardManager;

    [Inject]
    private void Inject(ABoardManager bm)
    {
        boardManager = bm;
    }

    [SerializeField]
    private float minDeltaBetweenSounds = .1f;
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip clip;

    private bool cursorActive;
    [Binding]
    public bool CursorActive
    {
        get { return cursorActive; }
        set { SetProperty(ref cursorActive, value, nameof(CursorActive)); }
    }

    private float lastSound = 0;

    private GraphNode node;
    public GraphNode Node
    {
        get { return node; }
        set
        {
            if (node != value)
            {
                node = value;
                Walkable = node.Walkable || boardManager.OccupiedNodes.Contains(node);
                OnNodeChanged();
            }
        }
    }

    private bool walkable;
    [Binding]
    public bool Walkable
    {
        get { return walkable; }
        private set
        {
            SetProperty(ref walkable, value, "Walkable");
        }
    }

    [Binding]
    public void OnNodeChanged()
    {
        if (Time.time - lastSound > minDeltaBetweenSounds && audioSource.enabled)
        {
            audioSource.PlayOneShot(clip);
            lastSound = Time.time;
        }
    }

    private void Start()
    {
        Debug.Log("New GridNodeHolder");
    }

    private void OnEnable()
    {
        CursorActive = true;
    }

    private void OnDisable()
    {
        CursorActive = false;
    }
}