

using System.Collections.Generic;
using Pathfinding;
using System.Linq;
using UnityEngine;
using System.Threading.Tasks;
using System;

internal class ExitVehicle : Ability
{
    public override string Name
    {
        get { return "Exit Vehicle"; }
    }

    public override int MinRange
    {
        get { return 1; }
    }

    public override int MaxRange
    {
        get { return 1; }
    }

    public override async Task ActivateAsync(params GraphNode[] affected)
    {
        var vehicle = character as Vehicle;
        if (!vehicle)
        {
            throw new Exception("ExitVehicle ability attached to a character that is not a vehicle");
        }
        if (!vehicle.Pilot)
        {
            throw new Exception("ExitVehicle ability attached to a vehicle with no pilot");
        }
        var c = vehicle.Pilot;
        c.gameObject.SetActive(true);
        _boardManager.AddCharacter(vehicle.Pilot, (Vector3)affected[0].position);
        c.HasMoved = true;
        c.transform.position = (Vector3)affected[0].position;
        vehicle.Pilot = null;
        await Task.FromResult(0);
    }

    public override bool CanActivate()
    {
        var graph = AstarData.active.data.gridGraph;
        var ret = graph.nodes.Where(node =>
        {
            var dist = graph.Distance(node, _boardManager.GetNodeFromCharacter(character));
            return MinRange <= dist && MaxRange >= dist;
        })
        .Any(node =>
        {
            return ValidTarget(node);
        });
        return ret;
    }

    public override bool ValidTarget(params GraphNode[] affected)
    {
        return affected.Length == 1 &&
            affected[0].Walkable &&
            !_boardManager.OccupiedNodes.Contains(affected[0]);
    }
}