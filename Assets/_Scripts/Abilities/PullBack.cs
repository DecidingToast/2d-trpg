﻿using Pathfinding;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityWeld.Binding;

internal class PullBack : Ability
{
    private GridGraph _graph;

    protected override void Start()
    {
        base.Start();
        _graph = AstarData.active.data.gridGraph;
    }

    public override async Task ActivateAsync(params GraphNode[] affected)
    {
        var target = _boardManager.GetCharacterFromNode(affected[0]);
        var targetNode = _boardManager.GetNodeFromCharacter(target);
        var charNode = _boardManager.GetNodeFromCharacter(character);
        Int3 direction = -(targetNode.position - charNode.position);

        // Character moves first to make its space walkable...
        await character.MoveTo(_graph.GetNearest((Vector3)(charNode.position + direction)).node)
            .ContinueWith(antecedent => target.MoveTo(charNode), TaskContinuationOptions.ExecuteSynchronously)
            .ContinueWith(antecedent => character.HasMoved = true, TaskContinuationOptions.ExecuteSynchronously);
    }

    public override bool CanActivate()
    {
        var validTargets = false;
        foreach (var c in _boardManager.GetAllCharacters(Team.PLAYER))
        {
            if (isCharacterValidTarget(c))
            {
                validTargets = true;
                break;
            }
        }
        return validTargets;
    }

    public override bool ValidTarget(params GraphNode[] affected)
    {
        return Array.TrueForAll(affected, node => isCharacterValidTarget(_boardManager.GetCharacterFromNode(node)));
    }

    private bool isCharacterValidTarget(ACharacter c)
    {
        var charNode = _boardManager.GetNodeFromCharacter(character) as GridNodeBase;
        var targetNode = _boardManager.GetNodeFromCharacter(c) as GridNodeBase;
        if (targetNode != null && charNode != null)
        {
            bool otherSideClear = false;
            for (int i = 0; i < 4; i++)
            {
                var opposite = getOppositeNeighbor(charNode, i);
                if (targetNode == charNode.GetNeighbourAlongDirection(i) && opposite != null && opposite.Walkable && !_boardManager.OccupiedNodes.Contains(opposite))
                {
                    otherSideClear = true;
                }
            }

            // Is the character adjacent and of the same team as character, and is there space to move?
            return (_graph.Distance(charNode, targetNode) == 1) &&
                (c.Team == character.Team) &&
                otherSideClear;
        }
        else
        {
            return false;
        }
    }

    private GridNodeBase getOppositeNeighbor(GridNodeBase node, int dir)
    {
        return node.GetNeighbourAlongDirection((dir + 2) % 4);

        // The reason this works (from pathfinding project documentation)
        // https://arongranberg.com/astar/documentation/4_1_10_fc5d3e1d/gridnodebase.html
        //     6  2  5
        //      \ | /
        //--3 - X - 1---- - X
        //      / | \
        //     7  0  4
    }
}
