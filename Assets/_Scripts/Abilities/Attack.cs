﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Pathfinding;
using UnityEngine;
using UnityWeld.Binding;

// TODO: Change this to pull stats/animations from character and equipped weapon.
[Binding]
internal class Attack : Ability
{
    [Binding]
    public override string Name
    {
        get
        {
            return "Atk: " + character?.Weapon?.Name;
        }
    }

    [Binding]
    public override int MinRange
    {
        get { return character?.Weapon?.MinRange ?? 1; }
    }

    [Binding]
    public override int MaxRange
    {
        get { return character?.Weapon?.MaxRange ?? 1; }
    }

    [Binding]
    public override Sprite Icon
    {
        get { return character?.Weapon?.Icon; }
    }

    // private Effect effect;
    private Vector3 effectPosition;

    public override async Task ActivateAsync(params GraphNode[] affected)
    {
        await attackAnim(_boardManager.GetCharacterFromNode(affected[0]));
        return;
    }

    private IEnumerator attackAnim(ACharacter affected)
    {
        yield return null;
        var xDiff = affected.transform.position.x - transform.position.x;
        // Face the character towards its target (reverse x scale). Assumes that characters root object scale is 1, and that the character starts facing left.
        var childScale = transform.GetChild(0).localScale;
        var newX = xDiff > 0 ?
            -Mathf.Abs(childScale.x) :
            Mathf.Abs(childScale.x);
        transform.GetChild(0).localScale = new Vector3(newX, childScale.y, childScale.y);
        affected.transform.GetChild(0).localScale = new Vector3(-newX, childScale.y, childScale.y);

        var charAtk = character.Weapon.DamType == Weapon.DamageType.PHYSICAL ? character.CurrStr : character.CurrMag;
        var atk = character.Weapon.Mt + charAtk;
        affected.TakeDamage(atk);
        character.HasMoved = true;
    }

    public override bool CanActivate()
    {
        if (character.Weapon == null)
        {
            return false;
        }

        Team targetTeam;
        switch (character.Team)
        {
            case Team.PLAYER:
            case Team.ALLY:
                targetTeam = Team.ENEMY | Team.NEUTRAL;
                break;
            case Team.ENEMY:
                targetTeam = Team.ALLY | Team.PLAYER;
                break;
            case Team.NEUTRAL:
                targetTeam = Team.ENEMY | Team.ALLY | Team.PLAYER;
                break;
            default:
                Debug.LogError($"Characters must be on exactly one of the four defined teams.");
                return false;
        }

        foreach (var target in _boardManager.GetAllCharacters(targetTeam))
        {
            var dist = AstarData.active.data.gridGraph.Distance(_boardManager.GetNodeFromCharacter(target), _boardManager.GetNodeFromCharacter(character));
            if (dist <= MaxRange && dist >= MinRange)
            {
                return true;
            }
        }
        return false;
    }

    public override bool ValidTarget(params GraphNode[] affected)
    {
        var target = _boardManager.GetCharacterFromNode(affected[0]);
        return affected.Length == 1 && target && target.Team != character.Team;
    }

    // public void PlayEffectSound()
    // {
    //     if (effect)
    //         effect.PlaySound();
    // }

    // public void PlayEffectAnimation()
    // {
    //     if (effect)
    //         effect.PlayAnimation();
    // }
}
