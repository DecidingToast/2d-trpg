﻿using Pathfinding;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;
using System;
using UnityWeld.Binding;
using System.Threading.Tasks;

/// <summary>
/// Represents activated ability of a character (e.g. attack, push, etc.)
/// </summary>
[Binding]
public abstract class Ability : DataBindMonobehaviour
{
    [Binding]
    public virtual string Name { get { return Stats?.Name; } }
    [Binding]
    public virtual int MaxRange { get { return Stats?.MaxRange ?? 0; } }
    [Binding]
    public virtual int MinRange { get { return Stats?.MinRange ?? 0; } }
    [Binding]
    public virtual Sprite Icon { get { return Stats?.Icon; } }

    [SerializeField]
    protected AbilityStats stats;
    [Binding]
    public AbilityStats Stats
    {
        get { return stats; }
        set
        {
            if (SetProperty(ref stats, value, nameof(Stats)))
            {
                OnPropertyChanged(nameof(Name));
                OnPropertyChanged(nameof(MaxRange));
                OnPropertyChanged(nameof(MinRange));
                OnPropertyChanged(nameof(Icon));
            }
        }
    }

    [Inject]
    protected ABoardManager _boardManager;
    protected ACharacter character;

    protected virtual void Awake()
    {
        character = GetComponent<ACharacter>();
    }

    protected virtual void Start() { }

    public abstract Task ActivateAsync(params GraphNode[] affected);

    public abstract bool ValidTarget(params GraphNode[] affected);

    public abstract bool CanActivate();
}
