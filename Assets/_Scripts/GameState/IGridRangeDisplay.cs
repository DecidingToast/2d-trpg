using System;
using UnityEngine;

internal interface IRangeDisplay
{
    void DisplayRange(int minRange, int maxRange, Func<Vector3> positionViewModifier);
    void HideRange();
}