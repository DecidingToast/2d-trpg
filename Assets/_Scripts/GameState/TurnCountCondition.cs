using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "TurnCountCondition", menuName = "ScriptableObjects/StateConditions/TurnCount")]
internal class TurnCountCondition : AStateCondition
{
    private enum Comparer
    {
        LESS,
        LESS_EQUAL,
        EQUAL,
        GREATER_EQUAL,
        GREATER
    }

    [SerializeField]
    private int turnCount;

    [SerializeField]
    Comparer comparison;

    [Inject]
    private ATurnState turnState;

    public override bool IsConditionMet(ControlContext oldContext, ControlContext newContext)
    {
        Debug.Log(turnState);
        var count = turnState.TurnCount;
        switch (comparison)
        {
            case Comparer.LESS:
                return count < turnCount;
            case Comparer.LESS_EQUAL:
                return count <= turnCount;
            case Comparer.EQUAL:
                return count == turnCount;
            case Comparer.GREATER_EQUAL:
                return count >= turnCount;
            case Comparer.GREATER:
                return count > turnCount;
            default:
                return true;
        }
    }
}