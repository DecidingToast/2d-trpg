using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "CharacterCountCondition", menuName = "ScriptableObjects/StateConditions/CharacterCount")]
internal class TeamCountCondition : AStateCondition
{
    private enum Comparer
    {
        LESS,
        LESS_EQUAL,
        EQUAL,
        GREATER_EQUAL,
        GREATER
    }

    [SerializeField]
    private int remainingCharacters;

    [SerializeField]
    private Team team;

    [SerializeField]
    private Comparer comparison;

    [Inject]
    ABoardManager boardManager;

    public override bool IsConditionMet(ControlContext oldContext, ControlContext newContext)
    {
        var count = boardManager.GetAllCharacters(team).Count;
        switch (comparison)
        {
            case Comparer.LESS:
                return count < remainingCharacters;
            case Comparer.LESS_EQUAL:
                return count <= remainingCharacters;
            case Comparer.EQUAL:
                return count == remainingCharacters;
            case Comparer.GREATER_EQUAL:
                return count >= remainingCharacters;
            case Comparer.GREATER:
                return count > remainingCharacters;
            default:
                return true;
        }
    }
}