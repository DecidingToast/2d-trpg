﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ABoardManager
{
    /// <summary>
    /// Gets all occupied nodes
    /// </summary>
    public abstract HashSet<GraphNode> OccupiedNodes { get; protected set; }
    /// <summary>
    /// Return all characters on the specified team (default: any)
    /// </summary>
    public abstract List<ACharacter> GetAllCharacters(Team team = Team.PLAYER | Team.ALLY | Team.ENEMY | Team.NEUTRAL);
    /// <summary>
    /// Returns all interactables on the map.
    /// </summary>
    public abstract List<Interactable> GetAllInteractables();
    /// <summary>
    /// Sets the most recent character to occupy a node
    /// </summary>
    public abstract void SetCharacterAtNode(ACharacter c, GraphNode n);
    /// <summary>
    /// Gets the last node a character moved onto
    /// </summary>
    public abstract GraphNode GetNodeFromCharacter(ACharacter c);
    /// <summary>
    /// Gets the most recent character to occupy an occupied node
    /// </summary>
    public abstract ACharacter GetCharacterFromNode(GraphNode n);
    /// <summary>
    /// Get the node occupied by the given interactable.
    /// </summary>
    public abstract GraphNode GetNodeFromInteractable(Interactable interactable);
    /// <summary>
    /// Moved given character to opposite team.
    /// </summary>
    public abstract void SwitchTeams(ACharacter c);
    /// <summary>
    /// Adds a character to the board state, at the given position.
    /// </summary>
    public abstract void AddCharacter(ACharacter c, Vector3 position);
    /// <summary>
    /// Removes a character from the board state.
    /// </summary>
    public abstract void RemoveCharacter(ACharacter c);
}
