﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal abstract class AObjectPool<T>
{
    private List<T> available = new List<T>();

    private List<T> inUse = new List<T>();

    public T[] Retrieve(int count)
    {
        var ret = new T[count];
        for (int i = 0; i < count; i++)
        {
            lock (available)
            {
                T obj;
                if (available.Count != 0)
                {
                    obj = available[0];
                    available.RemoveAt(0);
                }
                else
                {
                    obj = GetNewObject();
                }
                inUse.Add(obj);
                ret[i] = obj;
            }
        }
        return ret;
    }

    public void Release(params T[] objects)
    {
        foreach (var obj in objects)
        {
            inUse.Remove(obj);
            available.Add(obj);
            TearDown(obj);
        }
    }

    protected abstract T GetNewObject();

    protected abstract void TearDown(T obj);
}
