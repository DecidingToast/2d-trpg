using System;
using UnityEngine;
using Zenject;

internal class BattleInstaller : MonoInstaller<BattleInstaller>
{
    [SerializeField]
    private string groundUIPrefabPath;

    public override void InstallBindings()
    {
        var prefabPool = new PrefabPool(groundUIPrefabPath);
        Container.QueueForInject(prefabPool);

        Container.Bind<ATurnState>()
            .To<TurnManager>()
            .AsSingle();
        Container.Bind(typeof(ABoardManager), typeof(IInitializable))
            .To<GridBoardManager>()
            .AsSingle()
            .NonLazy();
        Container.Bind<AObjectPool<GameObject>>()
            .To<PrefabPool>()
            .FromInstance(prefabPool)
            .AsSingle()
            .NonLazy();
        Container.Bind(typeof(ICharacterSelectState), typeof(IInitializable))
            .To<CharacterSelectState>()
            .AsSingle()
            .NonLazy();
    }
}
