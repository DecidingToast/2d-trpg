﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using Zenject;

internal class CharacterSlot : MonoBehaviour
{
    [SerializeField]
    private ACharacter _character;
    public ACharacter Character { get { return _character; } set { _character = value; } }

    void Start()
    {
        var g = AstarData.active.data.gridGraph;
        transform.position = (Vector3)g.GetNearest(transform.position).node.position;
    }
}
