﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal interface ICharacterSelectState
{
    List<ACharacter> GetAllCharacters();
    void AddCharacters(params ACharacter[] characters);
    List<ACharacter> GetGenerics(ACharacter character);
    bool IsCharacterSelected(ACharacter character);
    void SelectCharacter(ACharacter character, CharacterSlot slot);
    void DeselectCharacter(ACharacter character);
    int SelectedCount { get; }

    List<CharacterSlot> Slots { get; }
}
