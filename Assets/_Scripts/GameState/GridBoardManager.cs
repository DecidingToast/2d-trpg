﻿using System;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;
using Zenject;

internal sealed class GridBoardManager : ABoardManager, IInitializable
{
    private Dictionary<GridNode, ACharacter> _nodesToCharacters = new Dictionary<GridNode, ACharacter>();
    private Dictionary<ACharacter, GridNode> _charactersToNodes = new Dictionary<ACharacter, GridNode>();
    private HashSet<GraphNode> _occupiedNodes = new HashSet<GraphNode>();
    private List<ACharacter> players = new List<ACharacter>();
    private List<ACharacter> enemies = new List<ACharacter>();
    private List<ACharacter> neutral = new List<ACharacter>();
    private List<ACharacter> allies = new List<ACharacter>();

    public override HashSet<GraphNode> OccupiedNodes
    {
        get
        {
            return _occupiedNodes;
        }
        protected set
        {
            _occupiedNodes = value;
        }
    }

    public override GraphNode GetNodeFromCharacter(ACharacter c)
    {
        if (c)
        {
            GridNode ret = null;
            if (_charactersToNodes.ContainsKey(c))
                ret = _charactersToNodes[c];
            return ret;
        }
        return null;
    }

    public override ACharacter GetCharacterFromNode(GraphNode n)
    {
        ACharacter ret = null;
        if (_nodesToCharacters.ContainsKey(n as GridNode))
            ret = _nodesToCharacters[n as GridNode];
        return ret;
    }

    public override void SetCharacterAtNode(ACharacter c, GraphNode n)
    {
        if (_charactersToNodes.ContainsKey(c))
        {
            var oldNode = _charactersToNodes[c];
            _nodesToCharacters.Remove(oldNode);
            OccupiedNodes.Remove(oldNode);
            oldNode.Walkable = true;
        }

        OccupiedNodes.Add(n);
        if (_turnManager.Team != c.Team)
        {
            n.Walkable = false;
        }
        else
        {
            n.Walkable = true;
        }
        _nodesToCharacters[n as GridNode] = c;
        _charactersToNodes[c] = n as GridNode;
    }

    [Inject]
    private ATurnState _turnManager;

    public override List<ACharacter> GetAllCharacters(Team team = Team.PLAYER | Team.ALLY | Team.ENEMY | Team.NEUTRAL)
    {
        var ret = new List<ACharacter>();
        if (team.HasFlag(Team.PLAYER))
        {
            ret.AddRange(players);
        }
        if (team.HasFlag(Team.ENEMY))
        {
            ret.AddRange(enemies);
        }
        if (team.HasFlag(Team.ALLY))
        {
            ret.AddRange(allies);
        }
        if (team.HasFlag(Team.NEUTRAL))
        {
            ret.AddRange(neutral);
        }
        return ret;
    }

    public void Initialize()
    {
        _turnManager.PhaseChanged += OnTurnChanged;
        var characters = GameObject.FindObjectsOfType<ACharacter>();
        foreach (var character in characters)
        {
            switch (character.Team)
            {
                case Team.PLAYER:
                    players.Add(character);
                    break;
                case Team.ENEMY:
                    enemies.Add(character);
                    break;
                case Team.ALLY:
                    allies.Add(character);
                    break;
                case Team.NEUTRAL:
                    neutral.Add(character);
                    break;
                default:
                    Debug.LogError("Character not on a valid team");
                    break;
            }
        }
    }

    public override List<Interactable> GetAllInteractables()
    {
        return new List<Interactable>(GameObject.FindObjectsOfType<Interactable>()); // Objects may not stay interactable. We should not cache this.
    }

    public override GraphNode GetNodeFromInteractable(Interactable interactable)
    {
        return AstarData.active.data.gridGraph.GetNearest(interactable.transform.position).node; // This should not be called often, no need to cache.
    }

    public override void SwitchTeams(ACharacter c)
    {
        switch (c.Team)
        {
            case Team.ALLY:
                allies.Remove(c);
                break;
            case Team.ENEMY:
                enemies.Remove(c);
                break;
            case Team.NEUTRAL:
                neutral.Remove(c);
                break;
            default:
                Debug.LogError($"Characters can only switch teams from ALLY, ENEMY, or NEUTRAL to PLAYER");
                return;
        }
        players.Add(c);
        c.Team = Team.PLAYER;
        GetNodeFromCharacter(c).Walkable = _turnManager.Team == Team.PLAYER;
    }

    private void UpdateNodesWalkable()
    {
        foreach (var pair in _charactersToNodes)
        {
            var c = pair.Key;
            var n = pair.Value;
            switch (c.Team)
            {
                case Team.PLAYER:
                case Team.ALLY:
                    n.Walkable = _turnManager.Team == Team.ALLY || _turnManager.Team == Team.PLAYER;
                    break;
                case Team.NEUTRAL:
                    n.Walkable = _turnManager.Team == Team.NEUTRAL;
                    break;
                case Team.ENEMY:
                    n.Walkable = _turnManager.Team == Team.ENEMY;
                    break;
            }
        }
    }

    private void OnTurnChanged(object sender, ATurnState.PhaseChangeEventArgs args)
    {
        UpdateNodesWalkable();
    }

    public override void AddCharacter(ACharacter c, Vector3 position)
    {
        switch (c.Team)
        {
            case Team.PLAYER:
                players.Add(c);
                break;
            case Team.ALLY:
                allies.Add(c);
                break;
            case Team.ENEMY:
                enemies.Add(c);
                break;
            case Team.NEUTRAL:
                neutral.Add(c);
                break;
            default:
                Debug.LogError($"Characters must belong to one of ALLY, ENEMY, NEUTRAL, or PLAYER teams");
                return;
        }
        SetCharacterAtNode(c, AstarData.active.data.gridGraph.GetNearest(position).node);
    }

    public override void RemoveCharacter(ACharacter c)
    {
        var node = GetNodeFromCharacter(c) as GridNode;
        if (OccupiedNodes.Contains(node))
        {
            OccupiedNodes.Remove(GetNodeFromCharacter(c));
        }
        _nodesToCharacters.Remove(node);
        _charactersToNodes.Remove(c);
        node.Walkable = true;
        switch (c.Team)
        {
            case Team.PLAYER:
                players.Remove(c);
                break;
            case Team.ALLY:
                allies.Remove(c);
                break;
            case Team.ENEMY:
                enemies.Remove(c);
                break;
            case Team.NEUTRAL:
                neutral.Remove(c);
                break;
            default:
                Debug.LogError($"Characters must belong to one of ALLY, ENEMY, NEUTRAL, or PLAYER teams");
                return;
        }
    }
}
