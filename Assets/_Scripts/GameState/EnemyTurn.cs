using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

internal class EnemyTurn : ControlContext
{
    private List<ACharacter> enemies;

    [Inject]
    ATurnState turnManager;

    protected override void OnEnable()
    {
        base.OnEnable();
        turnManager.Team = Team.ENEMY;
        RunEnemyTurn()
            .ContinueWith(antecedent =>
            {
                contextStack.PopContext();
            }, TaskContinuationOptions.ExecuteSynchronously);
    }

    private async Task RunEnemyTurn()
    {
        await EnemyTurnDeferred();
    }

    private IEnumerator EnemyTurnDeferred()
    {
        yield return new WaitForSeconds(5);
    }
}