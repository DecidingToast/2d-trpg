internal class TurnManager : ATurnState
{
    private Team team;
    public override Team Team
    {
        get
        {
            return team;
        }

        set
        {
            if (value != Team)
            {
                var old = team;
                team = value;
                OnPhaseChanged(new PhaseChangeEventArgs
                {
                    OldTeam = old,
                    NewTeam = team
                });
                if (value == Team.PLAYER)
                {
                    ++TurnCount;
                }
            }
        }
    }

    public override int TurnCount { get; protected set; }
}