using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "SceneChangeTriggeredEvent", menuName = "ScriptableObjects/TriggeredEvents/SceneChange")]
internal class SceneChangeTriggeredEvent : ATriggeredEvent
{
    [SerializeField]
    private string nextScene;

    public override async Task TriggerEvent()
    {
        Debug.Log("Level complete");
        await SceneManager.LoadSceneAsync(nextScene, LoadSceneMode.Single);
    }
}