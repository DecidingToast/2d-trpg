﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

/// <summary>
/// Conditions are checked before any panel transition. If all conditions are met, the result is triggered.
/// </summary>
internal class StateBasedTrigger : MonoBehaviour
{
    private enum LogicalOperator
    {
        AND,
        OR
    }

    [SerializeField]
    private AStateCondition[] conditions;

    [SerializeField]
    private ATriggeredEvent[] triggeredEvents;

    [SerializeField]
    private LogicalOperator combinationOperator;

    [Inject]
    private AControlStack controlStack;

    [Inject]
    DiContainer container;

    void OnEnable()
    {
        foreach (var condition in conditions)
        {
            container.Inject(condition);
        }
        foreach (var triggeredEvent in triggeredEvents)
        {
            container.Inject(triggeredEvent);
        }
        controlStack.ContextChanged += AControlStack_ContextChanged;
    }

    void OnDisable()
    {
        controlStack.ContextChanged -= AControlStack_ContextChanged;
    }

    void AControlStack_ContextChanged(object sender, AControlStack.ContextChangeEventArgs args)
    {
        var conditionsMet = false;
        switch (combinationOperator)
        {
            case LogicalOperator.AND:
                conditionsMet = conditions.All(condition => condition.IsConditionMet(args.OldContext, args.NewContext));
                break;
            case LogicalOperator.OR:
                conditionsMet = conditions.Any(condition => condition.IsConditionMet(args.OldContext, args.NewContext));
                break;
        }

        if (conditionsMet)
        {
            Debug.Log($"Conditions met for state based trigger {gameObject.name}");
        }
    }
}
