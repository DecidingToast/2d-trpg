using System;

public abstract class ATurnState
{
    public class PhaseChangeEventArgs : EventArgs
    {
        public Team OldTeam { get; set; }
        public Team NewTeam { get; set; }
        public int TurnCount { get; set; }
    }

    public event EventHandler<PhaseChangeEventArgs> PhaseChanged;

    public abstract Team Team { get; set; }

    public abstract int TurnCount { get; protected set; }

    protected virtual void OnPhaseChanged(PhaseChangeEventArgs args)
    {
        PhaseChanged?.Invoke(this, args);
    }
}