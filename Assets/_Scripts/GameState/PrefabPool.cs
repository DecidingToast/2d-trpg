﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

internal class PrefabPool : AObjectPool<GameObject>
{
    private readonly string prefabPath;

    // Must be lazy-initialized because prefabLocator will not be injected at construction time.
    private GameObject template;
    private GameObject Template
    {
        get
        {
            if (template == null)
            {
                template = prefabLocator.GetAsset(prefabPath);
            }
            return template;
        }
    }

    [Inject]
    IAssetLocator<GameObject> prefabLocator;

    protected override GameObject GetNewObject()
    {
        return GameObject.Instantiate(Template);
    }

    protected override void TearDown(GameObject obj)
    {
        obj.SetActive(false);
        obj.transform.position = Vector3.zero;
    }

    public PrefabPool(string prefabPath)
    {
        this.prefabPath = prefabPath;
    }
}
