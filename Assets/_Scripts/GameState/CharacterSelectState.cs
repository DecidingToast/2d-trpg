﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Linq;
using System;
using System.Threading.Tasks;

internal class CharacterSelectState : ICharacterSelectState, IInitializable
{
    [Inject]
    private ICharacterModel characterModel;
    [Inject]
    private ABoardManager boardManager;
    [Inject]
    private IAssetLocator<GameObject> prefabLocator;
    [Inject]
    DiContainer container;
    private List<ACharacter> primary = new List<ACharacter>();
    private Dictionary<CharacterSlot, ACharacter> selected = new Dictionary<CharacterSlot, ACharacter>();
    private Dictionary<ACharacter, List<ACharacter>> generic = new Dictionary<ACharacter, List<ACharacter>>();

    public int SelectedCount { get { return selected.Count; } }

    public List<CharacterSlot> Slots { get; private set; }

    public List<ACharacter> GetAllCharacters()
    {
        List<ACharacter> ret = new List<ACharacter>();

        foreach (var c in primary)
        {
            ret.Add(c);
            ret.AddRange(GetGenerics(c));
        }
        return ret;
    }

    /// <summary>
    /// Adds the given ICharacters, with the first as a primary and the following ICharacters as subordinates of the first.
    /// </summary>
    public void AddCharacters(params ACharacter[] ICharacters)
    {
        primary.Add(ICharacters[0]);
        generic[ICharacters[0]] = new List<ACharacter>();
        for (int i = 1; i < ICharacters.Length; i++)
        {
            generic[ICharacters[0]].Add(ICharacters[i]);
        }
    }

    /// <summary>
    /// Return a list of all generic ICharacters subordinate to the given primary ICharacter.
    /// </summary>
    public List<ACharacter> GetGenerics(ACharacter c)
    {
        return generic[c] != null ? generic[c] : new List<ACharacter>();
    }

    public void Initialize()
    {
        characterModel.PlayerCharacters.ForEach(c =>
        {
            var prefabAsset = prefabLocator.GetAsset(c.Prefab);
            if (prefabAsset == null)
            {
                Debug.LogError($"Unable to find prefab asset with name \"{c.Prefab}\".");
                return;
            }
            GameObject prefab = GameObject.Instantiate(prefabLocator.GetAsset(c.Prefab));
            if (!prefab)
            {
                Debug.LogError($"Unable to load prefab: {c.Prefab}");
                return;
            }
            ACharacter character = prefab.GetComponent<ACharacter>();
            if (!character)
            {
                Debug.LogError($"Prefab {c.Prefab} does not have a component at the top level that derives from ACharacter");
                return;
            }

            container.InjectGameObject(prefab);
            AddCharacters(character);
            character.SetStats(c);
            character.gameObject.SetActive(false);
        });
        Slots = GameObject.FindObjectsOfType<CharacterSlot>().ToList();
    }

    public void SelectCharacter(ACharacter character, CharacterSlot slot)
    {
        if (GetAllCharacters().Contains(character))
        {
            boardManager.AddCharacter(character, slot.transform.position);
            if (selected.ContainsKey(slot) && selected[slot])
            {
                DeselectCharacter(selected[slot]);
            }
            selected[slot] = character;
            slot.Character = character;
            character.transform.position = slot.transform.position;
            character.gameObject.SetActive(true);
        }
        else
        {
            Debug.LogError($"Character must be registered with CharacterSelectState before it can be selected");
        }
    }

    public void DeselectCharacter(ACharacter character)
    {
        var keys = selected.Keys.ToArray();
        foreach (var key in keys)
        {
            if (selected[key] == character)
            {
                key.Character = null;
                boardManager.RemoveCharacter(character);
                selected.Remove(key);
                character.gameObject.SetActive(false);
            }
        }
    }

    public bool IsCharacterSelected(ACharacter character)
    {
        return selected.Values.Contains(character);
    }
}
