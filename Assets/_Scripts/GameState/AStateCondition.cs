
using UnityEngine;

internal abstract class AStateCondition : ScriptableObject
{
    public abstract bool IsConditionMet(ControlContext oldContext, ControlContext newContext);
}