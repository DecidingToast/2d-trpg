using System.Threading.Tasks;
using UnityEngine;

internal abstract class ATriggeredEvent : ScriptableObject
{
    public abstract Task TriggerEvent();
}