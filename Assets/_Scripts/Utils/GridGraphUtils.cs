using System;
using Pathfinding;
using UnityEngine;

public static class GridGraphUtils
{
    public static int Distance(this GridGraph graph, GraphNode a, GraphNode b)
    {
        var aGrid = a as GridNodeBase;
        var bGrid = b as GridNodeBase;
        int ax = aGrid.NodeInGridIndex % graph.Width;
        int ay = aGrid.NodeInGridIndex / graph.Width;
        int bx = bGrid.NodeInGridIndex % graph.Width;
        int by = bGrid.NodeInGridIndex / graph.Width;

        return Math.Abs(ax - bx) + Math.Abs(ay - by);
    }
}