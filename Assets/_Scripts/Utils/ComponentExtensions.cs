using System;
using UnityEngine;

static internal class ComponentExtensions
{
    /// <summary>
    /// Gets or add a component. Usage example:
    /// BoxCollider boxCollider = transform.GetOrAddComponent<BoxCollider>();
    /// </summary>
    static public T GetOrAddComponent<T>(this Component child) where T : Component
    {
        T result = child.GetComponent<T>();
        if (result == null)
        {
            result = child.gameObject.AddComponent<T>();
        }
        return result;
    }

    /// <summary>
    /// Gets or add a component. Usage example:
    /// BoxCollider boxCollider = transform.GetOrAddComponent(typeof(BoxCollider));
    /// </summary>
    static public Component GetOrAddComponent(this Component child, Type type)
    {
        if (type == null)
        {
            throw new ArgumentNullException("Type parameter cannot be null");
        }
        if (!typeof(Component).IsAssignableFrom(type))
        {
            Debug.LogWarning($"Type {type} does not derive from Component");
            return null;
        }
        var result = child.GetComponent(type);
        if (result == null)
        {
            result = child.gameObject.AddComponent(type);
        }
        return result;
    }
}