using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pathfinding;
using UnityEngine;
using UnityWeld.Binding;
using Zenject;

[Binding]
public abstract class ACharacter : DataBindMonobehaviour
{
    #region DEPENDENCIES
    [Inject]
    private IRefDataProvider dataProvider;

    [Inject]
    DiContainer container;
    #endregion

    [SerializeField]
    private CharacterStats stats = new CharacterStats();
    [Binding]
    public virtual CharacterStats Stats
    {
        get
        {
            return stats;
        }
        private set
        {
            SetProperty(ref stats, value, nameof(Stats));
        }
    }

    #region CURRENT_STATS
    [SerializeField]
    private int currMov;
    [Binding]
    public virtual int CurrMov
    {
        get { return currMov; }
        protected set { SetProperty(ref currMov, value, nameof(CurrMov)); }
    }

    [SerializeField]
    private int currStr;
    [Binding]
    public virtual int CurrStr
    {
        get { return currStr; }
        protected set { SetProperty(ref currStr, value, nameof(CurrStr)); }
    }

    [SerializeField]
    private int currMag;
    [Binding]
    public virtual int CurrMag
    {
        get { return currMag; }
        protected set { SetProperty(ref currMag, value, nameof(CurrMag)); }
    }

    [SerializeField]
    private int currDef;
    [Binding]
    public virtual int CurrDef
    {
        get { return currDef; }
        protected set { SetProperty(ref currDef, value, nameof(currDef)); }
    }

    [SerializeField]
    private int currRes;
    [Binding]
    public virtual int CurrRes
    {
        get { return currRes; }
        set { SetProperty(ref currRes, value, nameof(CurrRes)); }
    }

    [SerializeField]
    private int currSpd;
    [Binding]
    public virtual int CurrSpd
    {
        get { return currSpd; }
        set { SetProperty(ref currSpd, value, nameof(CurrSpd)); }
    }

    [SerializeField]
    private int currHP;
    [Binding]
    public virtual int CurrHP
    {
        get { return currHP; }
        protected set { SetProperty(ref currHP, value, nameof(CurrHP)); }
    }

    [SerializeField]
    private int currSkl;
    [Binding]
    public virtual int CurrSkl
    {
        get { return currSkl; }
        set { SetProperty(ref currSkl, value, nameof(CurrSkl)); }
    }
    #endregion

    #region RUNTIME_STAT_TOTALS
    [SerializeField]
    private int totalMov;
    [Binding]
    public virtual int TotalMov
    {
        get { return totalMov; }
        protected set { SetProperty(ref totalMov, value, nameof(TotalMov)); }
    }

    [SerializeField]
    private int totalStr;
    [Binding]
    public virtual int TotalStr
    {
        get { return totalStr; }
        protected set { SetProperty(ref totalStr, value, nameof(TotalStr)); }
    }

    [SerializeField]
    private int totalMag;
    [Binding]
    public virtual int TotalMag
    {
        get { return totalMag; }
        protected set { SetProperty(ref totalMag, value, nameof(TotalMag)); }
    }

    [SerializeField]
    private int totalDef;
    [Binding]
    public virtual int TotalDef
    {
        get { return totalDef; }
        protected set { SetProperty(ref totalDef, value, nameof(totalDef)); }
    }

    [SerializeField]
    private int totalRes;
    [Binding]
    public virtual int TotalRes
    {
        get { return totalRes; }
        set { SetProperty(ref totalRes, value, nameof(TotalRes)); }
    }

    [SerializeField]
    private int totalSpd;
    [Binding]
    public virtual int TotalSpd
    {
        get { return totalSpd; }
        set { SetProperty(ref totalSpd, value, nameof(TotalSpd)); }
    }

    [SerializeField]
    private int totalHP;
    [Binding]
    public virtual int TotalHP
    {
        get { return totalHP; }
        protected set { SetProperty(ref totalHP, value, nameof(TotalHP)); }
    }

    [SerializeField]
    private int totalSkl;
    [Binding]
    public virtual int TotalSkl
    {
        get { return totalSkl; }
        set { SetProperty(ref totalSkl, value, nameof(TotalSkl)); }
    }
    #endregion

    [SerializeField]
    private Team team;
    [Binding]
    public Team Team
    {
        get { return team; }
        set
        {
            if (!Enum.IsDefined(typeof(Team), value))
                Debug.Log("A character cannot be on the ANY team.");
            else
                SetProperty(ref team, value, nameof(Team));
        }
    }

    [SerializeField]
    private string _name;
    [Binding]
    public virtual string Name
    {
        get { return _name; }
        protected set { SetProperty(ref _name, value, nameof(Name)); }
    }

    [SerializeField]
    private bool hasMoved;
    [Binding]
    public virtual bool HasMoved
    {
        get { return hasMoved; }
        set { SetProperty(ref hasMoved, value, nameof(HasMoved)); }
    }

    private Weapon weapon;
    [Binding]
    public virtual Weapon Weapon
    {
        get { return weapon; }
        protected set { SetProperty(ref weapon, value, nameof(Weapon)); }
    }

    private CharacterClass characterClass;
    [Binding]
    public CharacterClass CharacterClass
    {
        get { return characterClass; }
        private set { SetProperty(ref characterClass, value, nameof(CharacterClass)); }
    }

    private GrowthRates growthRates;
    [Binding]
    public virtual GrowthRates GrowthRates
    {
        get { return growthRates; }
        private set { SetProperty(ref growthRates, value, nameof(GrowthRates)); }
    }

    protected virtual void Start()
    {
        SetStats(stats);
    }

    /// <summary>
    /// Initializes all data for this character based on the stats object passed in.
    /// Accesses RefData asset bundles to fill in information.
    /// Handles any errors that occur during this process, and is therefore async void.
    /// </summary>
    public async void SetStats(CharacterStats stats)
    {
        try
        {
            Stats = stats;
            var weaponTable = dataProvider.GetTableAsync<Weapon>();
            var abilityTable = dataProvider.GetTableAsync<AbilityStats>();
            var growthRateTable = dataProvider.GetTableAsync<GrowthRates>();
            var statSpreadTable = dataProvider.GetTableAsync<StatSpread>();
            var classTable = dataProvider.GetTableAsync<CharacterClass>();

            await Task.WhenAll(weaponTable, abilityTable, growthRateTable, statSpreadTable, classTable);

            Weapon = !string.IsNullOrEmpty(Stats.Weapon) ? weaponTable.Result[Stats.Weapon] : null;
            GrowthRates = !string.IsNullOrEmpty(Stats.GrowthRates) ? growthRateTable.Result[Stats.GrowthRates] : null;


            statSpreadTable.Result.TryGet(Stats.StatCaps, out var charStatCaps);
            classTable.Result.TryGet(Stats.CharacterClass, out var charClass);

            if (charStatCaps == null)
            {
                Debug.LogError($"Unable to find base stats object {Stats.StatCaps} in table.");
            }
            if (charClass == null)
            {
                Debug.LogError($"Unable to find character class object {Stats.CharacterClass} in table.");
            }
            if (charStatCaps != null && charClass != null)
            {
                var charClassBases = charClass.BaseStats;

                Name = Stats.Name;
                CurrHP = TotalHP = Stats.HP + charClassBases.HP;
                CurrStr = TotalStr = Stats.Str + charClassBases.Str;
                CurrMag = TotalMag = Stats.Mag + charClassBases.Mag;
                CurrDef = TotalDef = Stats.Def + charClassBases.Def;
                CurrRes = TotalRes = Stats.Res + charClassBases.Res;
                CurrSpd = TotalSpd = Stats.Spd + charClassBases.Spd;
                CurrSkl = TotalSkl = Stats.Skl + charClassBases.Skl;
                // TODO: May want to handle movement differently?
                CurrMov = TotalMov = Stats.Mov + charClassBases.Mov;
            }

            AddAbilities(Stats.Abilities, abilityTable.Result);
        }
        catch (Exception ex)
        {
            Debug.LogError($"Error setting up weapons and/or abilities for character {Name}:\n {ex}");
        }
    }

    private void AddAbilities(List<string> abilities, RefDataTable<AbilityStats> aTable)
    {
        abilities?.ForEach(id =>
        {
            var aStats = aTable[id];
            var t = Type.GetType(aStats.ClassName);
            if (t != null && t.IsSubclassOf(typeof(Ability)))
            {
                Ability a;
                if (!(a = GetComponent(t) as Ability))
                {
                    a = container.InstantiateComponent(t, gameObject) as Ability;
                }
                a.Stats = aStats;
            }
            else
            {
                Debug.LogError($"Ability class name {aStats.ClassName} does not refer to an ability type");
            }
        });
    }

    public virtual void LevelUp()
    {
        Stats.Str += UnityEngine.Random.Range(0f, 1f) < GrowthRates.Str ? 1 : 0;
        Stats.Mag += UnityEngine.Random.Range(0f, 1f) < GrowthRates.Mag ? 1 : 0;
        Stats.Def += UnityEngine.Random.Range(0f, 1f) < GrowthRates.Def ? 1 : 0;
        Stats.Res += UnityEngine.Random.Range(0f, 1f) < GrowthRates.Res ? 1 : 0;
        Stats.Skl += UnityEngine.Random.Range(0f, 1f) < GrowthRates.Skl ? 1 : 0;
        Stats.HP += UnityEngine.Random.Range(0f, 1f) < GrowthRates.HP ? 1 : 0;

        // TODO: Apply gain to current and total stats as well.
    }

    public abstract void Die();

    public abstract void TakeDamage(int damage);

    public abstract List<GraphNode> GetReachableTiles();

    public abstract void TeleportTo(GraphNode node);

    public abstract Task MoveTo(GraphNode node);
}