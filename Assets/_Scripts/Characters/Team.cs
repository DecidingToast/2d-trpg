﻿using System;

[Flags]
public enum Team
{
    PLAYER = 1,
    ALLY = 2,
    ENEMY = 4,
    NEUTRAL = 8
}
