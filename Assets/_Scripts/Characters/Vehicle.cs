﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;
using Zenject;

[RequireComponent(typeof(EnterVehicle), typeof(ExitVehicle))]
internal class Vehicle : Character
{
    // This is the portion of the pilot's skill that is added to a vehicle's stats
    private const float SKILL_MODIFIER = .5f;
    private Interactable enterVehicle;
    protected Interactable EnterVehicle
    {
        get
        {
            if (!enterVehicle)
            {
                enterVehicle = GetComponent<EnterVehicle>();
            }
            return enterVehicle;
        }
    }

    private Ability exitVehicle;
    protected Ability ExitVehicle
    {
        get
        {
            if (!exitVehicle)
            {
                exitVehicle = GetComponent<ExitVehicle>();
            }
            return exitVehicle;
        }
    }

    private ACharacter pilot;
    [Binding]
    public ACharacter Pilot
    {
        get { return pilot; }
        set
        {
            if (SetProperty(ref pilot, value, nameof(Pilot)))
            {
                if (pilot == null)
                {
                    EnterVehicle.enabled = true;
                    ExitVehicle.enabled = false;
                }
                else
                {
                    ExitVehicle.enabled = true;
                    EnterVehicle.enabled = false;
                }
                OnPropertyChanged(nameof(Name));
                OnPropertyChanged(nameof(CurrMov));
                OnPropertyChanged(nameof(CurrStr));
                OnPropertyChanged(nameof(CurrDef));
                OnPropertyChanged(nameof(CurrMag));
                OnPropertyChanged(nameof(CurrRes));
                OnPropertyChanged(nameof(CurrSpd));
                OnPropertyChanged(nameof(CurrSkl));
                OnPropertyChanged(nameof(HasMoved));
            }
        }
    }

    [Binding]
    public override string Name
    {
        get { return Pilot ? Pilot.Name : base.Name; }
    }
    [Binding]
    public override int CurrMov
    {
        get { return Pilot != null ? base.CurrMov : 0; }
    }
    [Binding]
    public override int CurrStr
    {
        get { return Pilot != null ? (int)(Pilot.CurrSkl * SKILL_MODIFIER) + base.CurrStr : 0; }
    }
    [Binding]
    public override int CurrMag
    {
        get { return Pilot != null ? (int)(Pilot.CurrSkl * SKILL_MODIFIER) + base.CurrMag : 0; }
    }
    [Binding]
    public override int CurrDef
    {
        get { return Pilot != null ? (int)(Pilot.CurrSkl * SKILL_MODIFIER) + base.CurrDef : 0; }
    }
    [Binding]
    public override int CurrRes
    {
        get { return Pilot != null ? (int)(Pilot.CurrSkl * SKILL_MODIFIER) + base.CurrRes : 0; }
    }
    [Binding]
    public override int CurrSpd
    {
        get { return Pilot != null ? (int)(Pilot.CurrSkl * SKILL_MODIFIER) + base.CurrSpd : 0; }
    }
    [Binding]
    public override int CurrSkl
    {
        get { return Pilot != null ? Pilot.CurrSkl : 0; }
    }
    [Binding]
    public override bool HasMoved
    {
        get { return Pilot == null || base.HasMoved; }
    }

    private int testDerivedBinding;
    [Binding]
    public int TestDerivedBinding
    {
        get { return testDerivedBinding; }
        set { SetProperty(ref testDerivedBinding, value, nameof(TestDerivedBinding)); }
    }
}
