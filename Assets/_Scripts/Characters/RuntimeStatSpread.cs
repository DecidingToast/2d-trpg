using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

/// <summary>
/// Used for base stats and stat caps in the following use cases:
/// - Character stat bases: These are the base stats for a character at level one, not including any bonuses. Supplemented by class stat bases.
/// - Class stat bases: These are added as a bonus on top of a character's stat bases, and should be MUCH smaller.
/// - Character stat caps: These are added as a bonus on top of a character's class stat caps, and should be MUCH smaller.
/// - Class stat caps: These are the maximum stats for characters in a class, not including bonuses. Supplemented by character stat caps.
/// </summary>
[Binding]
internal class RuntimeStatSpread : DataBindObject
{
    [SerializeField]
    private int hp;
    [Binding]
    public int HP
    {
        get { return hp; }
        set { SetProperty(ref hp, value, nameof(HP)); }
    }

    [SerializeField]
    private int str;
    [Binding]
    public int Str
    {
        get { return str; }
        set { SetProperty(ref str, value, nameof(Str)); }
    }

    [SerializeField]
    private int mag;
    [Binding]
    public int Mag
    {
        get { return mag; }
        set { SetProperty(ref mag, value, nameof(Mag)); }
    }

    [SerializeField]
    private int def;
    [Binding]
    public int Def
    {
        get { return def; }
        set { SetProperty(ref def, value, nameof(Def)); }
    }

    [SerializeField]
    private int res;
    [Binding]
    public int Res
    {
        get { return res; }
        set { SetProperty(ref res, value, nameof(Res)); }
    }

    [SerializeField]
    private int spd;
    [Binding]
    public int Spd
    {
        get { return spd; }
        set { SetProperty(ref spd, value, nameof(Spd)); }
    }

    [SerializeField]
    private int skl;
    [Binding]
    public int Skl
    {
        get { return skl; }
        set { SetProperty(ref skl, value, nameof(Skl)); }
    }

    [SerializeField]
    private int mov;
    [Binding]
    public int Mov
    {
        get { return mov; }
        set { SetProperty(ref mov, value, nameof(Mov)); }
    }

    public RuntimeStatSpread(StatSpread original)
    {
        HP = original.HP;
        Str = original.Str;
        Mag = original.Mag;
        Def = original.Def;
        Res = original.Res;
        Spd = original.Spd;
        Skl = original.Skl;
        Mov = original.Mov;
    }
}
