﻿using Pathfinding;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityWeld.Binding;
using System.Threading.Tasks;

[Binding]
internal partial class Character : ACharacter
{
    private bool isMoving;
    [Binding]
    public bool IsMoving
    {
        get { return isMoving; }
        set { SetProperty(ref isMoving, value, nameof(IsMoving)); }
    }

    [SerializeField]
    private float moveSpeed = 5f;

    private GridGraph _graph;
    #region DI
    private ATurnState turnManager;
    private ABoardManager boardManager;
    [Inject]
    private void init(ABoardManager boardManager, ATurnState turnManager)
    {
        this.turnManager = turnManager;
        this.boardManager = boardManager;
    }
    #endregion

    #region UNITY_METHODS
    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        _graph = AstarData.active.data.gridGraph;
        var node = _graph.GetNearest(transform.position).node;
        transform.position = (Vector3)node.position;
        if (turnManager != null) setNodeWalkable(turnManager.Team);
        if (boardManager != null) boardManager.SetCharacterAtNode(this, node);
    }
    #endregion

    #region MOVEMENT
    private void setNodeWalkable(Team args)
    {
        if (args != Team)
        {
            _graph.GetNearest(transform.position).node.Walkable = false;
        }
        else
        {
            _graph.GetNearest(transform.position).node.Walkable = true;
        }
    }

    public override List<GraphNode> GetReachableTiles()
    {

        var nodes = PathUtilities.BFS(_graph.GetNearest(transform.position).node, CurrMov);
        // from node in PathUtilities.BFS(_graph.GetNearest(transform.position).node, Stats.Mov)
        // where boardManager.GetCharacterFromNode(node) == null
        // select node;
        return nodes.ToList();
    }

    private float pathDistance(Vector3[] path)
    {
        var ret = 0f;
        for (int i = 1; i < path.Length; i++)
        {
            ret += Vector3.Distance(path[i], path[i - 1]);
        }
        return ret;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="node"></param>
    /// <param name="callback">Called when done moving, only if a path was found</param>
    /// <returns></returns>
    public override async Task MoveTo(GraphNode node)
    {
        if (node == boardManager.GetNodeFromCharacter(this))
        {
            return;
        }

        var graph = AstarData.active.data.gridGraph;
        ABPath path = ABPath.Construct(transform.position, (Vector3)node.position);

        // TODO: Support tag penalties and tag blocking
        //path.enabledTags = 
        //path.tagPenalties =

        AstarPath.StartPath(path);
        await path.WaitForPath();

        if (path.error || path.vectorPath.Count < 2)
        {
            throw new Exception("Error trying to find a path.");
        }

        var oldPos = transform.position;
        graph.GetNearest(transform.position).node.Walkable = true;
        var arr = new Vector3[path.vectorPath.Count + 2];
        path.vectorPath.CopyTo(arr, 1);
        arr[0] = arr[1];
        arr[arr.Length - 1] = arr[arr.Length - 2];
        arr[arr.Length - 1] = path.vectorPath[path.vectorPath.Count - 1];

        var taskSource = new TaskCompletionSource<bool>();
        IsMoving = true;
        LeanTween.moveSpline(gameObject, arr, pathDistance(arr) / moveSpeed)
            .setOrientToPath(true)
            .setEase(LeanTweenType.linear)
            .setOnComplete(() =>
            {
                taskSource.SetResult(true);
            });

        boardManager.SetCharacterAtNode(this, node);

        await taskSource.Task;
        IsMoving = false;
        Debug.Log("Done moving");
    }

    public override void TeleportTo(GraphNode node)
    {
        var oldPos = transform.position;
        transform.position = (Vector3)node.position;
        boardManager.SetCharacterAtNode(this, node);
    }
    #endregion

    public override void Die()
    {
        boardManager.RemoveCharacter(this);
        // TODO: Do an animation?
        gameObject.SetActive(false);
    }

    public override void TakeDamage(int damage)
    {
        CurrHP = Math.Max(CurrDef - damage, 0);

        if (CurrHP <= 0)
        {
            Die();
        }
    }
}
