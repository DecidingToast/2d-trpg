using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ProtoBuf;
using UnityEngine;
using UnityWeld.Binding;

[Binding]
[Serializable]
[ProtoContract]
public class CharacterStats : DataBindObject
{
    [SerializeField]
    private string prefab;
    [ProtoMember(1)]
    public string Prefab
    {
        get { return prefab; }
        set { prefab = value; }
    }

    [SerializeField]
    private string growthRates;
    [ProtoMember(2)]
    public string GrowthRates
    {
        get { return growthRates; }
        set { growthRates = value; }
    }

    [SerializeField]
    private string statCaps;
    [ProtoMember(3)]
    public string StatCaps
    {
        get { return statCaps; }
        set { statCaps = value; }
    }

    [SerializeField]
    private string characterClass;
    [ProtoMember(4)]
    public string CharacterClass
    {
        get { return characterClass; }
        set { characterClass = value; }
    }

    [SerializeField]
    private string name;
    [Binding]
    [ProtoMember(5)]
    public string Name
    {
        get { return name; }
        set { SetProperty(ref name, value, nameof(Name)); }
    }

    [SerializeField]
    private int mov;
    [Binding]
    [ProtoMember(6)]
    public int Mov
    {
        get { return mov; }
        set { SetProperty(ref mov, value, nameof(Mov)); }
    }

    [SerializeField]
    private int str;
    [Binding]
    [ProtoMember(7)]
    public int Str
    {
        get
        { return str; }
        set { SetProperty(ref str, value, nameof(Str)); }
    }

    [SerializeField]
    private int mag;
    [Binding]
    [ProtoMember(8)]
    public int Mag
    {
        get { return mag; }
        set { SetProperty(ref mag, value, nameof(Mag)); }
    }

    [SerializeField]
    private int def;
    [Binding]
    [ProtoMember(9)]
    public int Def
    {
        get { return def; }
        set { SetProperty(ref def, value, nameof(Def)); }
    }

    [SerializeField]
    private int spd;
    [Binding]
    [ProtoMember(10)]
    public int Spd
    {
        get { return spd; }
        set { SetProperty(ref spd, value, nameof(Spd)); }
    }

    [SerializeField]
    private int skl;
    [Binding]
    [ProtoMember(11)]
    public int Skl
    {
        get { return skl; }
        set { SetProperty(ref skl, value, nameof(Skl)); }
    }

    [SerializeField]
    private int res;
    [Binding]
    [ProtoMember(12)]
    public int Res
    {
        get { return res; }
        set { SetProperty(ref res, value, nameof(Res)); }
    }

    [SerializeField]
    private int hp;
    [Binding]
    [ProtoMember(13)]
    public int HP
    {
        get { return hp; }
        set { SetProperty(ref hp, value, nameof(HP)); }
    }

    [SerializeField]
    private int _lvl;
    [Binding]
    [ProtoMember(14)]
    public int Level
    {
        get { return _lvl; }
        set { SetProperty(ref _lvl, value, nameof(Level)); }
    }

    [SerializeField]
    private string portrait;
    [Binding]
    [ProtoMember(15)]
    public string Portrait
    {
        get { return portrait; }
        set { SetProperty(ref portrait, value, nameof(Portrait)); }
    }

    private string bust;
    [Binding]
    [ProtoMember(16)]
    public string Bust
    {
        get { return bust; }
        set { SetProperty(ref bust, value, nameof(Bust)); }
    }

    // Used for abilities that change character appearance (i.e. transformation, alternate mode, etc.)
    private string altPortrait;
    [Binding]
    [ProtoMember(17)]
    public string AltPortrait
    {
        get { return altPortrait; }
        set { SetProperty(ref altPortrait, value, nameof(AltPortrait)); }
    }

    // Used for abilities that change character appearance (i.e. transformation, alternate mode, etc.)
    private string altBust;
    [Binding]
    [ProtoMember(18)]
    public string AltBust
    {
        get { return altBust; }
        set { SetProperty(ref altBust, value, nameof(AltBust)); }
    }

    private string weapon;
    [Binding]
    [ProtoMember(19)]
    public string Weapon
    {
        get { return weapon; }
        set { SetProperty(ref weapon, value, nameof(Weapon)); }
    }

    private List<int> inventory;
    /// <summary>
    /// List of the inventory IDs of the objects in the character's inventory
    /// </summary>
    [Binding]
    [ProtoMember(20)]
    public List<int> Inventory
    {
        get { return inventory; }
        set { SetProperty(ref inventory, value, nameof(Inventory)); }
    }

    private List<string> abilities;
    [Binding]
    [ProtoMember(21)]
    public List<string> Abilities
    {
        get { return abilities; }
        set { SetProperty(ref abilities, value, nameof(Abilities)); }
    }
}