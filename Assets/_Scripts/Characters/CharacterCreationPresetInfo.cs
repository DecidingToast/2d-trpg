﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "PresetInfo", menuName = "ScriptableObjects/CharacterCreationPresetInfo")]
internal class CharacterCreationPresetInfo : ScriptableObject
{
    [SerializeField]
    private CharacterClass[] starterClasses;
    public CharacterClass[] StarterClasses
    {
        get { return starterClasses; }
    }
}
