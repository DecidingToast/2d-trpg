﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityWeld.Binding;

internal interface ISceneLoadService
{
    [Binding]
    bool InProgress { get; }

    float Progress { get; }

    /// <summary>
    /// Fired every frame while loading a scene, notifying about progress and completion of load.
    /// </summary>
    event EventHandler<SceneLoadEventArgs> SceneLoading;

    void LoadScene(string sceneName, Action<float> onUpdate, Action onCompleted);
}

internal class SceneLoadEventArgs
{
    public float Progress { get; private set; }
    public bool Complete { get; private set; }
    public SceneLoadEventArgs(float progress, bool complete)
    {
        Progress = progress;
        Complete = complete;
    }
}
