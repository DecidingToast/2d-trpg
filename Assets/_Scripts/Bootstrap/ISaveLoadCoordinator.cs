
using System;
using System.Collections.Generic;

internal interface ISaveLoadCoordinator
{
    event EventHandler<SaveLoadEventArgs> OnSave;
    event EventHandler<SaveLoadEventArgs> OnLoad;
    void Save();
    void Load();
}

internal class SaveLoadEventArgs : EventArgs
{
    public Dictionary<string, byte[]> Data { get; set; }
}