﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

[Binding]
public class TerrainModifier : ARefDataObject
{
    [SerializeField]
    private string tagName;
    [Binding]
    public string TagName { get { return tagName; } }

    [SerializeField]
    private int movPenalty;
    [Binding]
    public int MovPenalty { get { return movPenalty; } }

    [SerializeField]
    private bool walkable = true;
    [Binding]
    public bool Walkable { get { return walkable; } }

    [SerializeField]
    private float evasionBonus;
    [Binding]
    public float EvasionBonus { get { return evasionBonus; } }
}
