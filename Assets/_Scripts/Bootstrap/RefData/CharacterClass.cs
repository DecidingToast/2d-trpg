﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

[Binding]
public class CharacterClass : ARefDataObject
{
    [SerializeField]
    private string displayName;
    [Binding]
    public string DisplayName { get { return displayName; } }

    [SerializeField]
    private GrowthRates growthRates;
    [Binding]
    public GrowthRates GrowthRates { get { return growthRates; } }

    [SerializeField]
    private StatSpread baseStats;
    [Binding]
    public StatSpread BaseStats { get { return baseStats; } }

    [SerializeField]
    private StatSpread statCaps;
    [Binding]
    public StatSpread StatCaps { get { return statCaps; } }

    [SerializeField]
    private TerrainModifier[] terrainOverrides;
    [Binding]
    public TerrainModifier[] TerrainOverrides { get { return terrainOverrides; } }
}
