using UnityWeld.Binding;

[Binding]
public abstract class AInventoryItem : ARefDataObject
{
    private int id = 0;
    public int InventoryID
    {
        get
        {
            if (id == 0)
            {
                // Unique IDs are required for correct handling of Reference Data objects
                id = (this.GetType().ToString() + Name).GetHashCode();
            }
            return id;
        }
    }
}