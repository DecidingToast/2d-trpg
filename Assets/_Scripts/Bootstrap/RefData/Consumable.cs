using UnityEngine;
using UnityWeld.Binding;


[Binding]
internal class Consumable : AInventoryItem
{
    [SerializeField]
    private string displayName;
    [Binding]
    public string DisplayName { get { return displayName; } }

    [SerializeField]
    private StatSpread statModifiers;
    [Binding]
    public StatSpread StatModifiers { get { return statModifiers; } }

    [SerializeField]
    private int healing;
    [Binding]
    public int Healing { get { return healing; } }

    [SerializeField]
    private bool hasDuration;
    [Binding]
    public bool HasDuration { get { return hasDuration; } }

    [SerializeField]
    private int duration;
    [Binding]
    public int Duration { get { return duration; } }

    [SerializeField]
    private bool hasDurability;
    [Binding]
    public bool HasDurability { get { return hasDurability; } }

    [SerializeField]
    private int durability;
    [Binding]
    public int Durability { get { return durability; } }
}