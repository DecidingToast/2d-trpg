﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

/// <summary>
/// Used for character stat growths on level up in the following use cases:
/// - Character growth rates: These are the primary factor in determining the odds that a stat will increase on level up,
///   with all stats totaling around 300% combined.
/// 
/// - Class growth rates: These are supplemental to character growth rates, and should generally be much smaller,
///   with all stats totaling around 50% combined for base classes and 60% for advanced classes.
/// </summary>
[Binding]
public class GrowthRates : ARefDataObject
{
    [SerializeField]
    private float str;
    [Binding]
    public float Str { get { return str; } }

    [SerializeField]
    private float mag;
    [Binding]
    public float Mag { get { return mag; } }

    [SerializeField]
    private float def;
    [Binding]
    public float Def { get { return def; } }

    [SerializeField]
    private float res;
    [Binding]
    public float Res { get { return res; } }

    [SerializeField]
    private float skl;
    [Binding]
    public float Skl { get { return skl; } }

    [SerializeField]
    private float spd;
    [Binding]
    public float Spd { get { return spd; } }

    [SerializeField]
    private float hp;
    [Binding]
    public float HP { get { return hp; } }

    public static GrowthRates Sum(params GrowthRates[] modifiers)
    {
        var ret = new GrowthRates();
        foreach (var mod in modifiers)
        {
            ret.str += mod.str;
            ret.mag += mod.mag;
            ret.def += mod.def;
            ret.res += mod.res;
            ret.hp += mod.hp;
            ret.skl += mod.skl;
        }
        return ret;
    }

    public static GrowthRates CreateInstance(float hp, float str, float mag, float def, float res, float skl, float spd)
    {
        var ret = ScriptableObject.CreateInstance<GrowthRates>();
        ret.hp = hp;
        ret.str = str;
        ret.mag = mag;
        ret.def = def;
        ret.res = res;
        ret.skl = skl;
        ret.spd = spd;
        return ret;
    }
}
