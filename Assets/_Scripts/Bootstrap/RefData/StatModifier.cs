using System;
using UnityEngine;
using UnityWeld.Binding;

/// <summary>
/// Multiplicative modifiers are applied after additive modifiers
/// </summary>
[Binding]
internal class StatModifier : ARefDataObject
{
    #region ADDITIVE
    [SerializeField]
    private int str;
    [Binding]
    public int Str { get { return str; } }

    [SerializeField]
    private int mag;
    [Binding]
    public int Mag { get { return mag; } }

    [SerializeField]
    private int def;
    [Binding]
    public int Def { get { return def; } }

    [SerializeField]
    private int res;
    [Binding]
    public int Res { get { return res; } }

    [SerializeField]
    private int skl;
    [Binding]
    public int Skl { get { return skl; } }

    [SerializeField]
    private int hp;
    [Binding]
    public int HP { get { return hp; } }

    [SerializeField]
    private int mov;
    [Binding]
    public int Mov { get { return mov; } }
    #endregion

    #region MULTIPLICATIVE
    [SerializeField]
    private float strFactor;
    [Binding]
    public float StrFactor { get { return strFactor; } }

    [SerializeField]
    private float magFactor;
    [Binding]
    public float MagFactor { get { return magFactor; } }

    [SerializeField]
    private float defFactor;
    [Binding]
    public float DefFactor { get { return defFactor; } }

    [SerializeField]
    private float resFactor;
    [Binding]
    public float ResFactor { get { return resFactor; } }

    [SerializeField]
    private float sklFactor;
    [Binding]
    public float SklFactor { get { return sklFactor; } }

    [SerializeField]
    private float hpFactor;
    [Binding]
    public float HPFactor { get { return hpFactor; } }

    [SerializeField]
    private float movFactor;
    [Binding]
    public float MovFactor { get { return movFactor; } }
    #endregion


    [SerializeField]
    private int duration;
    [Binding]
    public int Duration { get { return duration; } }

    /// <summary>
    /// Does not carry over durations
    /// Additive modifiers are summed up
    /// Multiplicative modifiers are multiplied
    /// </summary>
    public static StatModifier Aggregate(params StatModifier[] modifiers)
    {
        var ret = new StatModifier();
        foreach (var mod in modifiers)
        {
            ret.str += mod.str;
            ret.mag += mod.mag;
            ret.def += mod.def;
            ret.res += mod.res;
            ret.hp += mod.hp;
            ret.mov += mod.mov;
            ret.skl += mod.skl;

            ret.strFactor *= mod.strFactor;
            ret.magFactor *= mod.magFactor;
            ret.defFactor *= mod.defFactor;
            ret.resFactor *= mod.resFactor;
            ret.hpFactor *= mod.hpFactor;
            ret.movFactor *= mod.movFactor;
            ret.sklFactor *= mod.sklFactor;
        }
        return ret;
    }
}