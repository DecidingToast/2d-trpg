﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Zenject;
using System.Threading.Tasks;
using System;

internal class RefDataTable<T> where T : ARefDataObject
{
    private IDictionary<string, T> objects = new Dictionary<string, T>();

    public virtual T this[string key]
    {
        get
        {
            return objects[key];
        }
    }

    public RefDataTable(IDictionary<string, T> dict)
    {
        this.objects = dict;
    }

    public bool TryGet(string key, out T value)
    {
        return objects.TryGetValue(key, out value);
    }
}
