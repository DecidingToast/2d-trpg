

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

/// <summary>
/// Data representing the properties of an ability
/// </summary>
[Binding]
[Serializable]
public class AbilityStats : ARefDataObject
{
    [SerializeField]
    private string displayName;
    [Binding]
    public string DisplayName { get { return displayName; } }

    [SerializeField]
    private string className;
    [Binding]
    public string ClassName { get { return className; } }

    [SerializeField]
    private int maxRange;
    [Binding]
    public int MaxRange { get { return maxRange; } }

    [SerializeField]
    private int minRange;
    [Binding]
    public int MinRange { get { return minRange; } }

    [SerializeField]
    private Sprite icon;
    [Binding]
    public Sprite Icon { get { return icon; } }

    [SerializeField]
    private int cooldown;
    [Binding]
    public int Cooldown { get { return cooldown; } }

    [SerializeField]
    private int uses;
    [Binding]
    public int Uses { get { return uses; } }

    // Ability target stat modifiers
    [Binding]
    public int Duration { get; }
    [Binding]
    public int HP { get; }
    [Binding]
    public int Str { get; }
    [Binding]
    public int Mag { get; }
    [Binding]
    public int Def { get; }
    [Binding]
    public int Mov { get; }
    [Binding]
    public int Skl { get; }
}