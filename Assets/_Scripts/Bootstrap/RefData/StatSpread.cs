using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

/// <summary>
/// Used for base stats and stat caps in the following use cases:
/// - Character stat bases: These are the base stats for a character at level one, not including any bonuses. Supplemented by class stat bases.
/// - Class stat bases: These are added as a bonus on top of a character's stat bases, and should be MUCH smaller.
/// - Character stat caps: These are added as a bonus on top of a character's class stat caps, and should be MUCH smaller.
/// - Class stat caps: These are the maximum stats for characters in a class, not including bonuses. Supplemented by character stat caps.
/// </summary>
[Binding]
public class StatSpread : ARefDataObject
{
    [SerializeField]
    private int hp;
    [Binding]
    public int HP { get { return hp; } }

    [SerializeField]
    private int str;
    [Binding]
    public int Str { get { return str; } }

    [SerializeField]
    private int mag;
    [Binding]
    public int Mag { get { return mag; } }

    [SerializeField]
    private int def;
    [Binding]
    public int Def { get { return def; } }

    [SerializeField]
    private int res;
    [Binding]
    public int Res { get { return res; } }

    [SerializeField]
    private int spd;
    [Binding]
    public int Spd { get { return spd; } }

    [SerializeField]
    private int skl;
    [Binding]
    public int Skl { get { return skl; } }

    [SerializeField]
    private int mov;
    [Binding]
    public int Mov { get { return mov; } }

    public static StatSpread CreateInstance(int hp, int str, int mag, int def, int res, int spd, int skl, int mov)
    {
        var ret = ScriptableObject.CreateInstance<StatSpread>();
        ret.hp = hp;
        ret.str = str;
        ret.mag = mag;
        ret.def = def;
        ret.res = res;
        ret.spd = spd;
        ret.skl = skl;
        ret.mov = mov;
        return ret;
    }

    public static StatSpread CreateCopy(StatSpread original)
    {
        var ret = ScriptableObject.CreateInstance<StatSpread>();
        ret.hp = original.hp;
        ret.str = original.str;
        ret.mag = original.mag;
        ret.def = original.def;
        ret.res = original.res;
        ret.spd = original.spd;
        ret.skl = original.skl;
        ret.mov = original.mov;
        return ret;
    }
}
