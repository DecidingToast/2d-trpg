﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;

[Binding]
public abstract class ARefDataObject : ScriptableObject
{
    public string Name
    {
        get { return name; }
    }
}
