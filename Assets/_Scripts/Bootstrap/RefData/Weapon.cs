﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;
using System;

/// <summary>
/// Scriptable object holding data about a weapon.
/// </summary>
[Binding]
public class Weapon : AInventoryItem
{
    //[JsonConverter(typeof(StringEnumConverter))]
    public enum DamageType
    {
        PHYSICAL,
        MAGIC
    }

    [SerializeField]
    private string displayName;
    [Binding]
    public string DisplayName { get { return displayName; } }
    [SerializeField]
    private int mt;
    [Binding]
    public int Mt { get { return mt; } }
    [SerializeField]
    private float acc;
    [Binding]
    public float Acc { get { return acc; } }
    [SerializeField]
    private int minRange;
    [Binding]
    public int MinRange { get { return minRange; } }
    [SerializeField]
    private int maxRange;
    [Binding]
    public int MaxRange { get { return maxRange; } }
    [SerializeField]
    private Sprite icon;
    [Binding]
    public Sprite Icon { get { return icon; } }
    [SerializeField]
    private float crit;
    [Binding]
    public float Crit { get { return crit; } }
    [SerializeField]
    private DamageType damType;
    [Binding]
    public DamageType DamType { get { return damType; } }
}
