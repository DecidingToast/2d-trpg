﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

internal class TestSaveLoad : MonoBehaviour
{

    [Inject]
    ISaveLoadCoordinator saveLoad;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.S))
        {
            saveLoad.Save();
            //.Then(() => Debug.Log("saved data"));
            Debug.Log("saved data");
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            saveLoad.Load();
            //.Then(() => Debug.Log("loaded data"));
            Debug.Log("loaded data");
        }
    }
}
