using System.Threading.Tasks;

internal interface IRefDataProvider
{
    Task<RefDataTable<T>> GetTableAsync<T>() where T : ARefDataObject;
}