﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityWeld.Binding;
using Zenject;

[Binding]
internal class LoadLevelScreenBlocker : DataBindMonobehaviour
{
    [SerializeField]
    private float progress;

    [SerializeField]
    CanvasGroup canvasGroup;

    [SerializeField]
    Animator animator;

    [SerializeField]
    private float fadeTime = .5f;
    private float time = 0f;

    private bool animating;

    [Binding]
    public float Progress
    {
        get { return progress; }
        private set { SetProperty(ref progress, value, nameof(Progress)); }
    }

    [SerializeField]
    private bool inProgress;
    [Binding]
    public bool InProgress
    {
        get { return inProgress; }
        private set { SetProperty(ref inProgress, value, nameof(InProgress)); }
    }

    [Inject]
    private ISceneLoadService sceneLoadService;

    private IEnumerator UpdateProgress()
    {
        InProgress = true;
        while (sceneLoadService.InProgress)
        {
            Progress = sceneLoadService.Progress;
            yield return null;
        }
        InProgress = false;
        Progress = 0;
    }

    private IEnumerator FadeIn()
    {
        animator.SetTrigger("FadeIn");
        animating = true;
        yield return new WaitUntil(() => !animating);
    }

    private void FinishAnimation()
    {
        animating = false;
    }

    private IEnumerator FadeOut(TaskCompletionSource<bool> source)
    {
        animator.SetTrigger("FadeOut");
        animating = true;
        yield return new WaitUntil(() => !animating);
    }

    private void ISceneLoadService_SceneLoading(object sender, SceneLoadEventArgs e)
    {
        if (!e.Complete)
        {
            StartCoroutine(UpdateProgress());
        }
    }

    public void LoadScene(string sceneName)
    {
        StartCoroutine(LoadSceneRoutine(sceneName));
    }

    private IEnumerator LoadSceneRoutine(string sceneName)
    {
        yield return FadeIn();
        sceneLoadService.SceneLoading += ISceneLoadService_SceneLoading;
        var source = new TaskCompletionSource<bool>();
        sceneLoadService.LoadScene(sceneName, progress => this.Progress = progress, () => StartCoroutine(FadeOut(source)));
    }
}
