﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityWeld.Binding;
using Zenject;

internal class AsyncSceneLoadService : DataBindObject, ISceneLoadService, IInitializable
{
    [Inject]
    MonoBehaviourHost monoHost;

    [Inject]
    ZenjectSceneLoader sceneLoader;

    public event EventHandler<SceneLoadEventArgs> SceneLoading;

    public bool InProgress { get; private set; }

    public float Progress { get; private set; }

    public void LoadScene(string levelName, Action<float> onUpdate, Action onCompleted)
    {
        if (InProgress)
        {
            Debug.LogError("Scene load currently in progress, cannot begin loading another.");
        }
        else
        {
            monoHost.Behaviour.StartCoroutine(LoadLevelAsync(levelName, onUpdate, onCompleted));
        }
    }

    private IEnumerator LoadLevelAsync(string levelName, Action<float> onUpdate, Action onCompleted)
    {
        InProgress = true;
        Progress = 0f;

        SceneLoading?.Invoke(this, new SceneLoadEventArgs(0, false));
        Debug.Log("Starting scene load");

        yield return null;

        var asyncScene = sceneLoader.LoadSceneAsync(levelName, LoadSceneMode.Single);

        // this stops the scene from displaying when it's finished loading
        asyncScene.allowSceneActivation = false;

        while (asyncScene.progress < 0.9f)
        {
            Progress = Mathf.Clamp01(asyncScene.progress);
            Debug.Log($"Scene load progress: {Progress}");
            SceneLoading?.Invoke(this, new SceneLoadEventArgs(Progress, false));
            onUpdate?.Invoke(Progress);
            yield return null;
        }

        asyncScene.allowSceneActivation = true;

        Progress = 1f;
        yield return null;
        InProgress = false;
        onCompleted?.Invoke();
        SceneLoading?.Invoke(this, new SceneLoadEventArgs(1, true));
    }

    public void Initialize()
    {
        Debug.Log("Using asynchronous scene loading");
    }
}
