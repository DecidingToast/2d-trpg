﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

internal class RefDataProvider : IInitializable, IRefDataProvider
{
    private class MonoBehaviourHost : MonoBehaviour { }

    private const string bundleName = "referencedata";

    private MonoBehaviour monoBehaviourHost;

    private Dictionary<Type, object> tables = new Dictionary<Type, object>();

    private AssetBundle bundle;

    public void Initialize()
    {
        monoBehaviourHost = new GameObject().AddComponent<MonoBehaviourHost>();
        bundle = AssetBundle.LoadFromFile(Path.Combine("Assets/_AssetBundles", bundleName));
        if (bundle == null)
        {
            Debug.LogError($"Unable to load asset bundle: {bundleName}");
        }
    }

    public Task<RefDataTable<T>> GetTableAsync<T>() where T : ARefDataObject
    {
        if (!tables.ContainsKey(typeof(T)))
        {
            Debug.Log($"Waiting for table {typeof(T)}Table");
            tables[typeof(T)] = WaitForTableAsync<T>();
        }
        return tables[typeof(T)] as Task<RefDataTable<T>>;
    }

    private async Task<RefDataTable<T>> WaitForTableAsync<T>() where T : ARefDataObject
    {
        var dict = new Dictionary<string, T>();
        await LoadAllAssetsAsync<T>(bundle, dict, () =>
        {
            Debug.Log($"Table loaded: {typeof(T)}Table");
        });
        return new RefDataTable<T>(dict);
    }

    private IEnumerator testAwait()
    {
        yield return null;
    }

    private static IEnumerator LoadAllAssetsAsync<U>(AssetBundle bundle, IDictionary table, Action callback) where U : ARefDataObject
    {
        if (bundle == null)
        {
            throw new Exception("No asset bundle provided to create table");
        }
        var request = bundle.LoadAllAssetsAsync<U>();
        yield return request; // Still have to do this in IEnumerator, because I can't have separate awaiters for single asset requests and multi-asset requests
        var assets = request.allAssets;
        if (assets == null || assets.Length == 0)
        {
            throw new Exception($"Failed to load assets for table: {typeof(U)}. Are there any assets of type {typeof(U)} currently in the asset bundles?");
        }

        foreach (var item in assets)
        {
            var asset = item as U;
            table[asset.Name] = asset;
        }

        callback?.Invoke();
    }
}
