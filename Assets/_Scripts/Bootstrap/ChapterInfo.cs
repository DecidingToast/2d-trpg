
using UnityEngine;

public class ChapterInfo : ARefDataObject
{
    [SerializeField]
    private string campaignName;
    public string CampaignName
    {
        get { return campaignName; }
    }

    [SerializeField]
    private string sceneName;
    public string SceneName
    {
        get { return sceneName; }
    }

    [SerializeField]
    private string[] prerequisites;
    /// <summary>
    /// These chapters must be completed before this one is unlocked
    /// </summary>
    public string[] Prerequisites
    {
        get { return prerequisites; }
    }

    [SerializeField]
    private string[] postrequisites;
    /// <summary>
    /// If any of these chapters are completed, this one can no longer be completed
    /// </summary>
    public string[] Postrequisites
    {
        get { return prerequisites; }
    }
}