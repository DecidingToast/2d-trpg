using System;
using System.IO;
using ProtoBuf;
using UnityEngine;

internal class ProtoObjectSerializer : IObjectSerializer
{
    public byte[] Serialize<T>(T data) where T : class
    {
        if (data == null)
        {
            return null;
        }
        try
        {
            using (var stream = new MemoryStream())
            {
                Serializer.Serialize(stream, data);
                return stream.ToArray();
            }
        }
        catch (Exception ex)
        {
            Debug.LogError($"Unable to serialize record of type {typeof(T)}: {ex.Message}");
            return null;
        }
    }

    public T Deserialize<T>(byte[] data) where T : class
    {
        if (data == null)
        {
            return null;
        }
        try
        {
            using (var stream = new MemoryStream(data))
            {
                return Serializer.Deserialize<T>(stream);
            }
        }
        catch (Exception ex)
        {
            Debug.LogError($"Unable to deserialize record of type {typeof(T)}: {ex.Message}");
            return null;
        }
    }
}