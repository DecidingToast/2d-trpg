using System.Collections.Generic;
using Zenject;

internal interface ICampaignModel
{
    Dictionary<string, (ChapterInfo, bool)> StageCompletion { get; }
}
