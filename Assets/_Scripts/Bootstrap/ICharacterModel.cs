using System.Collections.Generic;

internal interface ICharacterModel
{
    List<CharacterStats> PlayerCharacters { get; }
}
