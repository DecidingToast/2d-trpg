﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Zenject;

internal class CharacterModel : DataBindObject, ICharacterModel, IInitializable
{
    [Inject]
    private ISaveLoadCoordinator saveLoadCoordinator;
    [Inject]
    private IObjectSerializer serializer;

    private List<CharacterStats> playerCharacters = new List<CharacterStats>
    {
        new CharacterStats
        {
            Name = "Test",
            Str = 5,
            Mag = 6,
            Def = 3,
            Skl = 10,
            Mov = 5,
            HP = 18,
            Level = 3,
            Weapon = "Pistol",
            Bust = "Characters/Annika/AnnikaBust1024.png",
            Portrait = "Characters/Annika/AnnikaPortrait512.png",
            Prefab = "Units/Character.prefab",
            Abilities = new List<string> { "Attack" }
        },
        new CharacterStats
        {
            Name = "Test2",
            Str = 5,
            Mag = 6,
            Def = 3,
            Skl = 10,
            Mov = 5,
            HP = 18,
            Level = 3,
            Weapon = "Pistol",
            Bust = "Characters/Horned/HornedBust1024.png",
            Portrait = "Characters/Horned/HornedPortrait512.png",
            Prefab = "Units/Character.prefab",
            Abilities = new List<string> { "Attack" }
        }
    };

    public List<CharacterStats> PlayerCharacters
    {
        get { return playerCharacters; }
        private set { SetProperty(ref playerCharacters, value, nameof(PlayerCharacters)); }
    }

    public void Initialize()
    {
        playerCharacters = new List<CharacterStats>();
        saveLoadCoordinator.OnLoad += Load;
        saveLoadCoordinator.OnSave += Save;
    }

    private void Load(object sender, SaveLoadEventArgs args)
    {
        try
        {
            var data = args.Data[Enum.GetName(typeof(SaveLoadCoordinator.SaveLoadContext), SaveLoadCoordinator.SaveLoadContext.CHARACTERS)];
            PlayerCharacters = serializer.Deserialize<List<CharacterStats>>(data);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

    private void Save(object sender, SaveLoadEventArgs args)
    {
        try
        {
            var data = serializer.Serialize(PlayerCharacters);
            args.Data.Add(Enum.GetName(typeof(SaveLoadCoordinator.SaveLoadContext), SaveLoadCoordinator.SaveLoadContext.CHARACTERS), data);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }
}
