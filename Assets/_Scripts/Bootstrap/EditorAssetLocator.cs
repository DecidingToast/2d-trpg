using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

internal class EditorAssetLocator<T> : IAssetLocator<T>, IInitializable where T : UnityEngine.Object
{
    private const string ASSET_BUNDLE_LOCATION = "Assets/_AssetBundles";

    internal EditorAssetLocator(string bundleName)
    {
        BundleName = bundleName;

    }
    protected string BundleName { get; private set; }

    private Dictionary<string, string> namesToPaths = new Dictionary<string, string>();

    public void Initialize()
    {
        var bundle = AssetBundle.LoadFromFile(Path.Combine(ASSET_BUNDLE_LOCATION, BundleName));
        if (bundle == null)
        {
            Debug.LogError($"Unable to load asset bundle: {BundleName}");
            return;
        }
        var paths = bundle.GetAllAssetNames();
        foreach (var path in paths)
        {
            namesToPaths[Path.GetFileNameWithoutExtension(path).ToLower()] = path;
        }
        bundle.Unload(true);
    }

    public T GetAsset(string fileName)
    {
        namesToPaths.TryGetValue(Path.GetFileNameWithoutExtension(fileName).ToLower(), out var path);
        var asset = AssetDatabase.LoadAssetAtPath<T>(path);

        return asset ?? default(T);


    }
}