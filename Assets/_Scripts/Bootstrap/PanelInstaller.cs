using UnityEngine;
using Zenject;

internal class PanelInstaller : MonoInstaller<PanelInstaller>
{
    [SerializeField]
    private AControlStack panelStack;

    public override void InstallBindings()
    {
        Container.Bind<AControlStack>()
            .FromInstance(panelStack);
    }
}