using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using ProtoBuf;

internal class SaveLoadCoordinator : ISaveLoadCoordinator
{
    private readonly string SaveLocation = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\My Games\TheNameOfThisGame\";
    private readonly string FileName = @"Save.sav";

    public enum SaveLoadContext
    {
        CHARACTERS
    }

    /// <summary>
    /// Subscribers to this event should not call Unity API, as this may not be executed in the Unity thread.
    /// </summary>
    public event EventHandler<SaveLoadEventArgs> OnSave;

    /// <summary>
    /// Subscribers to this event should not call Unity API, as this may not be executed in the Unity thread.
    /// </summary>
    public event EventHandler<SaveLoadEventArgs> OnLoad;

    public void Save()
    {
        try
        {
            Directory.CreateDirectory(SaveLocation);
            using (var stream = File.Create(SaveLocation + FileName))
            {
                var toSave = new Dictionary<string, byte[]>();
                OnSave?.Invoke(this, new SaveLoadEventArgs { Data = toSave });
                Serializer.Serialize(stream, toSave);
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Unable to save data: {e}");
        }
    }

    public async void SaveAsync()
    {
        await Task.Run(() =>
        {
            Save();
        });
    }

    public void Load()
    {
        try
        {
            using (var file = File.OpenRead(SaveLocation + FileName))
            {
                var data = Serializer.Deserialize<Dictionary<string, byte[]>>(file);
                OnLoad?.Invoke(this, new SaveLoadEventArgs { Data = data });
            }
        }
        catch (Exception e)
        {
            Debug.LogError($"Unable to load data: {e}");
        }
    }

    public async void LoadAsync()
    {
        await Task.Run(() =>
        {
            Load();
        });
    }
}

