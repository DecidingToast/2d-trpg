internal interface IAssetLocator<T> where T : UnityEngine.Object
{
    T GetAsset(string fileName);
}