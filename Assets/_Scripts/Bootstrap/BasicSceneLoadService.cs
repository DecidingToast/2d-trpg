using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

internal class BasicSceneLoadService : ISceneLoadService, IInitializable
{
    public bool InProgress => false;

    public float Progress => 0f;

#pragma warning disable 67
    public event EventHandler<SceneLoadEventArgs> SceneLoading;
#pragma warning restore 67

    [Inject]
    private ZenjectSceneLoader sceneLoader;

    public void Initialize()
    {
        Debug.Log("Using synchronous scene loading");
    }

    public void LoadScene(string sceneName, Action<float> onUpdate, Action onComplete)
    {
        //Debug.Log("WTF debugging");
        sceneLoader.LoadScene(sceneName);
        onComplete?.Invoke();
    }
}