using UnityEngine;

internal class MonoBehaviourHost
{
    private class MonoHost : MonoBehaviour
    {
        private void Start()
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private MonoBehaviour behaviour;
    public MonoBehaviour Behaviour
    {
        get
        {
            if (!behaviour)
            {
                behaviour = new GameObject().AddComponent<MonoHost>();
            }
            return behaviour;
        }
    }
}