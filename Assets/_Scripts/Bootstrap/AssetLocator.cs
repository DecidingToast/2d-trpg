using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

internal class AssetLocator<T> : IAssetLocator<T>, IInitializable where T : UnityEngine.Object
{
    private const string ASSET_BUNDLE_LOCATION = "Assets/_AssetBundles";

    protected string BundleName { get; private set; }

    private AssetBundle bundle;

    public AssetLocator(string bundleName)
    {
        BundleName = bundleName;
    }

    public void Initialize()
    {
        bundle = AssetBundle.LoadFromFile(Path.Combine(ASSET_BUNDLE_LOCATION, BundleName));
        if (bundle == null)
        {
            Debug.LogError($"Unable to load asset bundle: {BundleName}");
        }
    }

    public T GetAsset(string fileName)
    {
        return string.IsNullOrEmpty(fileName) ? default(T) : bundle.LoadAsset<T>(Path.Combine(fileName));
    }
}