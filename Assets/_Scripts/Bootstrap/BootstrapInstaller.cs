using System;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

internal class BootstrapInstaller : MonoInstaller<BootstrapInstaller>
{
    [SerializeField]
    private LoadLevelScreenBlocker loadLevelScreenBlocker;

    public override void InstallBindings()
    {
        Container.Bind<ISaveLoadCoordinator>()
            .To<SaveLoadCoordinator>()
            .AsSingle()
            .NonLazy();
        Container.Bind(typeof(ICharacterModel), typeof(IInitializable))
            .To<CharacterModel>()
            .AsSingle()
            .NonLazy();
        Container.Bind(typeof(IRefDataProvider), typeof(IInitializable))
            .To<RefDataProvider>()
            .AsSingle();
        Container.Bind<MonoBehaviourHost>()
            .To<MonoBehaviourHost>()
            .AsSingle();
        Container.Bind<ISceneLoadService>()
            .To<AsyncSceneLoadService>()
            .AsSingle();
        Container.Bind<IObjectSerializer>()
            .To<ProtoObjectSerializer>()
            .AsSingle();
        Container.Bind<LoadLevelScreenBlocker>()
            .FromInstance(loadLevelScreenBlocker)
            .AsSingle();

#if UNITY_EDITOR
        // When running in the unity editor, asset bundles can cause problems inspection objects such as animators.
        // Instead, we can load these assets using the asset database.
        Container.Bind(typeof(IAssetLocator<Sprite>), typeof(IInitializable))
            .To<EditorAssetLocator<Sprite>>()
            .AsSingle()
            .WithArguments("sprites");
        Container.Bind(typeof(IAssetLocator<GameObject>), typeof(IInitializable))
            .To<EditorAssetLocator<GameObject>>()
            .AsSingle()
            .WithArguments("prefabs");
#else
        Container.Bind(typeof(IAssetLocator<Sprite>), typeof(IInitializable))
            .To<AssetLocator<Sprite>>()
            .AsSingle()
            .WithArguments("sprites");
        Container.Bind(typeof(IAssetLocator<GameObject>), typeof(IInitializable))
            .To<AssetLocator<GameObject>>()
            .AsSingle()
            .WithArguments("prefabs");
#endif
    }
}
