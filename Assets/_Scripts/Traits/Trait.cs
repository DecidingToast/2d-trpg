﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityWeld.Binding;
using Zenject;

/// <summary>
/// Represents a passive ability that is automatically applied when applicable
/// </summary>
[Binding]
public abstract class Trait : DataBindMonobehaviour
{
    [Binding]
    public virtual string Name { get; protected set; }

    protected ACharacter Character { get; private set; }

    [Inject]
    private ATurnState turnState;

    protected virtual void Awake()
    {
        Character = GetComponent<ACharacter>();
    }

    protected virtual void OnEnable()
    {
        turnState.PhaseChanged += ATurnState_PhaseChanged;
    }

    protected virtual void OnDisable()
    {
        turnState.PhaseChanged -= ATurnState_PhaseChanged;
    }

    private void ATurnState_PhaseChanged(object sender, ATurnState.PhaseChangeEventArgs args)
    {
        OnTurnStart(args);
    }

    protected virtual void OnTurnStart(ATurnState.PhaseChangeEventArgs args) { }
}
